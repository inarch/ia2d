module Data.Bifoldable where

class Bifoldable p where
  bifold :: Monoid m => p m m -> m
  bifold = bifoldMap id id
  {-# INLINE bifold #-}

  bifoldMap :: Monoid m => (a -> m) -> (b -> m) -> p a b -> m
