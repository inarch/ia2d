module Data.List.Shuffle (shuffle) where

import Control.Monad
import Control.Monad.ST
import Data.Array.ST (STArray, newListArray, readArray, writeArray)
import Data.STRef (newSTRef, readSTRef, writeSTRef)
import System.Random

-- https://www.haskell.org/haskellwiki/Random_shuffle

-- | Randomly shuffle a list without the IO Monad
--   /O(N)/
shuffle :: RandomGen g => g -> [a] -> ([a],g)
shuffle gen xs = runST (do
        g <- newSTRef gen
        let randomRST lohi = do
              (a,s') <- randomR lohi <$> readSTRef g
              writeSTRef g s'
              return a
        ar <- newArray n xs
        xs' <- forM [1..n] $ \i -> do
                j <- randomRST (i,n)
                vi <- readArray ar i
                vj <- readArray ar j
                writeArray ar j vi
                return vj
        gen' <- readSTRef g
        return (xs',gen'))
  where
    n = length xs
    newArray :: Int -> [a] -> ST s (STArray s Int a)
    newArray l =  newListArray (1,l)
