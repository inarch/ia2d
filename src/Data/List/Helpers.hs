module Data.List.Helpers (
    replace
  , for
  , lengthLt
  , lengthGe
  ) where

for :: [a] -> (a -> b) -> [b]
for = flip map

replace :: Eq a => a -> a -> [a] -> [a]
replace x y = map f
  where f z | z == x    = y
            | otherwise = z

lengthGe :: Int -> [a] -> Bool
lengthGe 0 _      = True
lengthGe _ []     = False
lengthGe n (_:xs) = lengthGe (n - 1) xs

lengthLt :: Int -> [a] -> Bool
lengthLt n = not . lengthGe n
