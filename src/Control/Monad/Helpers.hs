module Control.Monad.Helpers where

import Control.Monad (when)

whenM :: IO Bool -> IO () -> IO ()
whenM p f = p >>= flip when f

whenJust :: Monad m => Maybe a -> (a -> m ()) -> m ()
whenJust (Just x) f = f x
whenJust Nothing  _ = return ()

whenRight :: Monad m => Either a b -> (b -> m ()) -> m ()
whenRight = flip (either (const (return ())))

untilM :: Monad m => m Bool -> m ()
untilM f = do
  b <- f
  if b
    then return ()
    else untilM f

iterateM :: Monad m => Int -> m Bool -> m Bool
iterateM 0 _ = return False
iterateM n f = do
  b <- f
  if b
    then return True
    else iterateM (n - 1) f
