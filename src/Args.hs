module Args (
    -- * Generic helpers
    filePath

    -- * Options type
  , Options
  , defaultOptions
  , MInput(..)
  , showMInput

    -- * Helpers
  , options
  , applyFlags
  , getOptions
  , readGridSize
  ) where

import           Automaton.Migration.Options
import           Automaton.Types

import           Data.Text                   (Text)
import           Data.Tuple                  (swap)
import           System.Console.GetOpt
import           System.Environment          (getArgs)
import           System.FilePath             (splitExtension)
import           Text.Read                   (readMaybe)

type Options = MInput Text

defaultOptions :: Options
defaultOptions = MInput {
    migrationOpts = defaultMigrationOptions
  , gridSize      = (24, 24)
  , miMaxSteps    = Nothing
  , ruleset       = mempty
  , writeToStdout = False
  , svgPath       = Nothing
  , initGen       = Nothing
  , verbose       = True
  , feedFlag      = False
  , keepOutput    = False
  , liberalVars   = False
  , enterRepl     = False
  , skipContrPass = False
  }

int :: (Int -> Options -> Options) -> String -> Options -> Maybe Options
int f s o = f <$> readMaybe s <*> pure o

float :: (Float -> Options -> Options) -> String -> Options -> Maybe Options
float f s o = f <$> readMaybe s <*> pure o

-- | Read grid size value in "<width>x<height>" format.
readGridSize :: String -> Maybe (Int,Int)
readGridSize size =
  case break (== 'x') size of
    (w, 'x':h) -> Just (read w :: Int, read h :: Int)
    _          -> Nothing

options :: [OptDescr (Options -> Maybe Options)]
options =
    [ Option "" ["grid-size"] (ReqArg gridSize "<width>x<height>") "set grid size"
    , Option "" ["link-range"] (ReqArg maxDist "<distance>") "set maximum pointer distance"
    , Option "" ["hop-range"] (ReqArg migDist "<distance>") "set maximum migration distance"
    , Option "" ["spread-distance"] (ReqArg spreadDist "<distance>") "set spread distance"
    , Option "" ["stability"] (ReqArg stability "<stability>") "set migration stability"
    , Option "" ["diffusion"] (ReqArg diffusion "<diffusion>") "set migration diffusion"
    , Option "" ["tension"] (ReqArg tension "<tension>") "set migration tension"

    , Option "" ["steps"] (ReqArg maxSteps "<n>") "limit the number of steps to <n>"
    , Option "" ["svg"] (ReqArg svg "out-.svg") "write graphical output to files `out-<i>.svg`"
    , Option "" ["stdout"] (NoArg $ \o -> Just o { writeToStdout = True}) "write textual output to stdout"
    , Option "" ["seed"] (ReqArg gen "<n>") "seed value for random number generator"
    , Option "" ["quiet"] (NoArg $ \o -> Just o { verbose = False}) "less verbose output"
    , Option "" ["feed"] (NoArg $ \o -> Just o { feedFlag = True}) "feed all input before reduction"
    , Option "" ["keep"] (NoArg $ \o -> Just o { keepOutput = True}) "keep output cells on grid"
    , Option "" ["liberal-vars"] (NoArg $ \o -> Just o { liberalVars = True }) "allow liberal variable names"
    , Option "i" ["interactive"] (NoArg $ \o -> Just o { enterRepl = True }) "enter interactive mode"
    ]
  where
    withMigOpts :: (MigrationOptions -> MigrationOptions) -> Options -> Options
    withMigOpts f o = o { migrationOpts = f (migrationOpts o) }

    maxDist = int $ \i -> withMigOpts $ \o -> o { maxDist = i }
    migDist = int $ \i -> withMigOpts $ \o -> o { migDist = i }
    spreadDist = int $ \i -> withMigOpts $ \o -> o { spreadDist = i }

    tension = float $ \i -> withMigOpts $ \o -> o { tension = i }
    stability = float $ \i -> withMigOpts $ \o -> o { stability = i }
    diffusion = float $ \i -> withMigOpts $ \o -> o { diffusion = i }

    gridSize s o =
      case readGridSize s of
        Just size -> Just o { gridSize = swap size }
        Nothing -> Nothing

    maxSteps = int $ \i o -> o { miMaxSteps = Just i }

    svg s o = Just o { svgPath = Just s }

    gen s o = Just o { initGen = Just (read s) }

applyFlags :: [String] -> Options -> (Options, [String], [String])
applyFlags args opts =
  let (os, as, es) = getOpt Permute options args
  in
  case foldr (maybe Nothing) (Just opts) os of
    Just o  -> (o, as, es)
    -- TODO: signify ill-formed value (e.g. bad grid size)
    Nothing -> (opts, as, es)

-- | Get options from command-line arguments.
getOptions :: IO (Options, [String], [String])
getOptions = do
  args <- getArgs
  return $ applyFlags args defaultOptions

-- | Generate file name from a template.
--
-- Example:
--
-- >>> filePath "path/to/file-.txt" 42
-- "path/to/file-42.txt"
filePath :: FilePath -> String -> FilePath
filePath p i = name ++ i ++ ext
  where (name, ext) = splitExtension p
