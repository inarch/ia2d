module PotRedex (
    -- * Types
    Tag
  , Label(..)
  , Node(..)
  , Cell(..)
    -- * Constructor functions
  , empty, cell
  , soil, clean
  , node, wait, forward, input, output
    -- * Helpers
  , feed
  , tag
  , allCoords, allTags
  , updatePointers
  , splitRedex
    -- * Predicates
  , isRedex, isIORedex, isNormalRedex, isSpecialRedex, isForward, isOccupied
  , isInput, isOutput
  ) where

import           Data.Maybe (maybeToList)

import           Coord
import           Data.List.Helpers

-- * Types
type Tag = Int

data Label a
  = Forwarder
  | Input Tag
  | Waiter
  | Output Tag
  | Epsilon
  | Delta
  | Custom a
  deriving (Eq, Ord, Show)

data Node a =
  Node {
    label :: Label a
  , coords :: [AbsCoord]
  }
  deriving (Eq, Show)

instance (Eq a, Ord a) => Ord (Node a) where
  compare n1 n2 = compare (label n1) (label n2)

node :: Label a -> [AbsCoord] -> Node a
node = Node

wait :: Node a
wait = Node Waiter []

forward :: AbsCoord -> Node a
forward r = Node Forwarder [r]

input :: Tag -> Node a
input t = Node (Input t) []

output :: Tag -> Node a
output t = Node (Output t) []

isIO :: Node a -> Bool
isIO c = case label c of Input _ -> True; Output _ -> True; _ -> False

isInput :: Node a -> Bool
isInput c | Input _ <- label c = True
          | otherwise          = False

isOutput :: Node a -> Bool
isOutput c | Output _ <- label c = True
           | otherwise           = False

tag :: Node a -> Maybe Tag
tag c = case label c of Input t -> Just t; Output t -> Just t; _ -> Nothing

waiting :: Node a -> Bool
waiting c = case label c of Waiter -> True; _ -> False

forwarding :: Node a -> Bool
forwarding c = case label c of Forwarder -> True; _ -> False

occupied :: Node a -> Bool
occupied c = not (waiting c || forwarding c)

data Cell a =
  Cell {
    node1 :: Node a
  , node2 :: Node a
  , dirty :: Bool
  }
  deriving (Eq, Ord, Show)

-- * Constructor functions

empty :: Cell a
empty = Cell (Node Waiter []) (Node Waiter []) True

cell :: Node a -> Node a -> Cell a
cell n1 n2 = Cell n1 n2 True

soil :: Cell a -> Cell a
soil c = c { dirty = True }

clean :: Cell a -> Cell a
clean c = c { dirty = False }

-- * Helpers

-- | Place a node into a cell replacing a waiting node.
feed :: Cell a -> Node a -> Cell a
feed c n =
  case (label (node1 c), label (node2 c)) of
    (Waiter, _) -> c { node1 = n, dirty = True }
    (_, Waiter) -> c { node2 = n, dirty = True }
    _  -> error "redex full"

-- | Get list of all instances of coordinates in a cell.
allCoords :: Cell a -> [AbsCoord]
allCoords c  = coords (node1 c) ++ coords (node2 c)

allTags :: Cell a -> [Tag]
allTags c = maybeToList (tag (node1 c)) ++ maybeToList (tag (node2 c))

-- | Update all pointers to point to a new location.
updatePointers :: AbsCoord -> AbsCoord -> Cell a -> Cell a
updatePointers old new c =
  c { node1 = f (node1 c), node2 = f (node2 c) }
  where f (Node l r) = Node l (replace old new r)

splitRedex :: Cell a -> (Node a, Node a)
splitRedex c = (node1 c, node2 c)

-- * Predicates

isRedex :: Cell a -> Bool
isRedex c = not (waiting (node1 c) || waiting (node2 c))

isIORedex :: Cell a -> Bool
isIORedex c = isRedex c && (isIO (node1 c) || isIO (node2 c)) && not (forwarding (node1 c) || forwarding (node2 c))

isSpecialRedex :: Cell a -> Bool
isSpecialRedex c = isRedex c && not (isNormalRedex c)

isNormalRedex :: Cell a -> Bool
isNormalRedex c = isRedex c && not (isIO (node1 c) || isIO (node2 c) || forwarding (node1 c) || forwarding (node2 c))

isOccupied :: Cell a -> Bool
isOccupied c = occupied (node1 c) || occupied (node2 c)

isForward :: Cell a -> Bool
isForward (Cell (Node Forwarder _) (Node Waiter _) _) = True
isForward (Cell (Node Waiter _) (Node Forwarder _) _) = True
isForward _ = False
