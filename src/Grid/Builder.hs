module Grid.Builder (
    (@@)
  , cell
  , pre
  , (><)
  , setupGrid
  ) where

import Control.Monad.State (State, execState, modify)

import Coord (RelCoord)
import qualified Grid as G
import PotRedex

infix 5 @@

(@@) :: Cell a -> (Int, Int) -> State (G.Grid (Cell a)) ()
c @@ i = modify $ \g -> G.allocate g i c

pre :: Label a -> [RelCoord] -> Cell a
pre f fs = cell (node f fs) wait

infix 6 ><

(><) :: Node a -> Node a -> Cell a
n1 >< n2 = cell n1 n2

-- | Setup a grid state.
--
-- Suggested usage:
--
-- @
-- setupGrid (10,10) $ do
--   pre "f" [] @@ (2,1)
--   node "g" [] >< node "h" [] @@ (3,3)
--   pre "i" [] @@ (3,2)
-- @
setupGrid :: (Int, Int) -> State (G.Grid a) () -> G.Grid a
setupGrid dim = flip execState (G.create ((1, 1), dim))
