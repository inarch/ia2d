{-# LANGUAGE OverloadedStrings #-}

module Language.Pin.Error where

import Data.ByteString.Char8 (ByteString)
import Data.List (nub)
import Text.Parsec (ParseError, errorPos, sourceLine, sourceColumn)
import Text.Parsec.Error (errorMessages, Message(..))
import Data.String (IsString(fromString))

data LoadError a
  = LoadError String
  | ParseError String
  | RuleError String
  | InvalidInput
  | FileError FilePath (LoadError a)
  | ImportError FilePath (LoadError a)
  deriving (Eq, Show)

type InputError = LoadError ByteString

instance IsString (LoadError a) where
  fromString = LoadError

showError :: LoadError a -> String
showError e =
  case e of
    FileError path e' -> unlines $ ["Cannot load '" ++ path ++ "' due to an error:"] ++ map ("  " ++) (lines . showError $ e')
    ParseError s -> s
    RuleError s -> s
    LoadError s -> s
    InvalidInput -> "Invalid input"
    ImportError path (e'@(LoadError _)) -> unlines $ ["The import of file '" ++ path ++ "' failed:"] ++ map ("  " ++) (lines . showError $ e')
    ImportError path e' -> unlines $ ["In '" ++ path ++ "':"] ++ map ("  " ++) (lines . showError $ e')

enumerate :: String -> String -> [String] -> String
enumerate _     end [x1, x2] = x1 ++ end ++ x2
enumerate _     _   [x]      = x
enumerate delim end (x:xs)   = x ++ delim ++ enumerate delim end xs
enumerate _     _   []       = ""

partitionErrorMessages :: [Message] -> ([String], [String], [String])
partitionErrorMessages = go ([], [], [])
  where
    go (a, b, c) []     = (filter (not . null) . nub $ a, nub b, nub c)
    go (a, b, c) (x:xs) = case x of
                            UnExpect m    -> go (m:a, b, c) xs
                            SysUnExpect m -> go (m:a, b, c) xs
                            Expect m      -> go (a, m:b, c) xs
                            Message m     -> go (a, b, m:c) xs

fromParseError :: String -> ParseError -> LoadError a
fromParseError programText parseError =
    ParseError $ unlines
      [ "Parse error at line " ++ show line ++ ", column " ++ show column ++ ":"
      , "  " ++ errorLine
      , "  " ++ replicate (column - 1) ' ' ++ "^"
      , case (unexpected, expected) of
          (_:_, _:_) -> "Unexpected " ++ enumOr unexpected ++ ", expected " ++ enumOr expected
          ([],  _:_) -> "Expected " ++ enumOr expected
          (_:_, [])  -> "Unexpected " ++ enumOr unexpected
          _          -> ""
      ]
  where
    errorLine = let i = line - 1 in if length progLines > i then progLines !! i else head progLines
    progLines = lines programText
    line = sourceLine . errorPos $ parseError
    column = sourceColumn . errorPos $ parseError
    (unexpected, expected, _) = partitionErrorMessages . errorMessages $ parseError
    enumOr = enumerate ", " " or "
