{-# LANGUAGE FlexibleContexts #-}

module Language.Pin.Pin (
    Rule(..)
  , NormalRule(..)
  , Net(..)
  , Comp(..)

  , canonify, unflatten
  , allVars, boundVars, freeVars
  , ruleTerms, compTerms
  ) where

import           Data.Bifoldable
import           Data.Bifunctor
import           Data.Monoid       ((<>))
import           Data.MultiSet     (MultiSet)
import qualified Data.MultiSet     as MS

import           Language.Pin.Net
import           Language.Pin.Term (Term (..))

data Rule a b =
  -- initial terms should always contain variables
  -- todo: Rule [b] [b] (Net a b)
  Rule (Term a b) (Term a b) (Net a b) deriving (Eq, Ord, Show)

data NormalRule a b =
  -- links with a variable should not occur (except for loops)
  -- todo: NormalRule [Term a b] [Term a b] (Net a b)
  NormalRule (Term a b) (Term a b) (Net a b) deriving (Eq, Ord, Show)

instance Bifunctor Rule where
  bimap f g (Rule t u (Net ns)) = Rule (bimap f g t) (bimap f g u) (Net . map (bimap f g) $ ns)

instance Bifoldable Rule where
  bifoldMap f g (Rule t u (Net ns))
    = bifoldMap f g u <> bifoldMap f g t
                      <> mconcat (fmap (bifoldMap f g) ns)

allVars :: Ord b => [Comp a b] -> MultiSet b
allVars = MS.unions . map (bifoldMap (const mempty) MS.singleton)

freeVars :: Ord b => [Comp a b] -> [b]
freeVars = map fst . filter ((==) 1 . snd) . MS.toOccurList . allVars

boundVars :: Ord b => [Comp a b] -> [b]
boundVars = map fst . filter ((==) 2 . snd) . MS.toOccurList . allVars

replaceVar :: Eq b => (b, Term a b) -> Term a b -> Term a b
replaceVar (x, t) (V y) = if x == y then t else V y
replaceVar s (T f xs) = T f $ map (replaceVar s) xs

replaceVarN :: Eq b => (b, Term a b) -> [Comp a b] -> [Comp a b]
replaceVarN s = map (\(Comp t1 t2) -> Comp (replaceVar s t1) (replaceVar s t2))

construct :: (Eq b, Show a, Ord b, Show b) => [Comp a b] -> [Comp a b] -> [Comp a b]
construct [] l = reverse l
construct (Comp (V v) t : q) l | v `elem` allVars q =
  construct (replaceVarN (v, t) q) l
construct (Comp (V v) t : q) l | v `elem` allVars l =
  construct q (replaceVarN (v, t) l)
construct (Comp t (V v) : q) l = construct (Comp (V v) t : q) l
construct (n : q) l = construct q (n : l)

unflatten :: (Show a, Show b, Eq b, Ord b) => [Comp a b] -> [Comp a b]
unflatten ns = construct ns []

canonify :: (Show a, Show b, Eq b, Ord b) => Rule a b -> NormalRule a b
canonify (Rule t1 t2 (Net ns)) =
  case construct ns [Comp t1 t2] of
    Comp t1' t2' : q -> NormalRule t1' t2' (Net q)
    _                -> error "canonify"

ruleTerms :: Rule a b -> [Term a b]
ruleTerms (Rule a b (Net n)) = a : b : concatMap compTerms n
