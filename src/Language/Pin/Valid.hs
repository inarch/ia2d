{-# LANGUAGE FlexibleContexts #-}

module Language.Pin.Valid where

import           Control.Monad.Except
import           Data.Bifoldable
import           Data.List            (intercalate)
import qualified Data.Map             as M
import           Data.Monoid
import qualified Data.MultiSet        as MS
import qualified Data.Set             as S

import           Display.Pretty
import           Language.Pin.Error
import           Language.Pin.Net
import           Language.Pin.Pin
import           Language.Pin.Term

checkProg :: (MonadError (LoadError a) m, Pretty a, Pretty b, Ord a, Ord b, Show a, Show b) => [Rule a b] -> [Net a b] -> m ()
checkProg rules inputNets = do
  mapM_ checkRule rules
  mapM_ checkNet inputNets
  void $ checkArities rules inputNets

checkActivePairVars :: Ord b => Term a b -> Term a b -> Bool
checkActivePairVars t u = getAll $ foldMap All [
  -- t and u must be flat terms
    flat t
  , flat u
  -- all variables in t and u must occur exactly once,
  -- t and u must not have shared variables
  , Prelude.all ((== 1) . snd) (MS.toOccurList (MS.union a b))
  ]
  where
    a = bifoldMap (const mempty) MS.singleton t
    b = bifoldMap (const mempty) MS.singleton u

checkRule :: (MonadError (LoadError a) m, Pretty a, Pretty b, Ord a, Ord b) => Rule a b -> m ()
checkRule r@(Rule t u _) = do
    unless (checkActivePairVars t u) $ throwError . RuleError $ "The left-hand side of this rule contains nested nodes:\n  " ++ pretty r
    unless (getAll . foldMap (All . checkOccur 2) $ MS.toList vs) $ throwError . RuleError $ "Each variable in the following rule must occur exactly twice:\n  " ++ pretty r
  where
    vs = bifoldMap (const mempty) MS.singleton r
    vs' = MS.toMap vs
    checkOccur n v | Just o <- M.lookup v vs', o == n = True
                   | otherwise                        = False

checkNet :: (MonadError (LoadError a) m, Pretty a, Pretty b, Eq a, Ord b) => Net a b -> m ()
checkNet (Net cs) =
    unless (all ((<= 2) . snd) . MS.toOccurList . ms $ cs) $
      throwError . RuleError $ "Some variables occur more than twice:\n  " ++ intercalate ", " (map pretty cs)
  where
    ms = foldr (MS.union . bifoldMap (const mempty) MS.singleton) MS.empty

checkArities :: (MonadError (LoadError a) m, Pretty a, Ord a) => [Rule a b] -> [Net a b] -> m [(a, Int)]
checkArities rs ns =
    forM (M.toList . arities $ terms rs ns) $ \(k, v) ->
      case S.toList v of
        [x] -> return (k, x)
        _   -> throwError . LoadError $ "This function symbol is used with multiple arities: " ++ pretty k

terms :: Foldable f => f (Rule a b) -> f (Net a b) -> [Term a b]
terms rules inputNets =
    concatMap fromRule rules ++ concatMap (concatMap compTerms . components) inputNets
  where
    fromRule (Rule x y (Net cs)) = [x, y] ++ concatMap compTerms cs
