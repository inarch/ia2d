module Language.Pin.Term (
    Term(..)

  , depth
  , flat
  , subterms
  , rename, rename', renameWith

  , isTerm
  , isVar

  , Occur
  , vars, vars'
  , freeVars
  , boundVars
  , nestedVars

  , Arity
  , arities
  ) where

import           Data.Bifoldable
import           Data.Bifunctor
import           Data.List       (foldl')
import           Data.Map        (Map)
import qualified Data.Map        as M
import           Data.Maybe      (mapMaybe)
import qualified Data.Set        as S

data Term a b
  = V b
  | T a [Term a b]
  deriving (Eq, Ord, Show)

instance Bifunctor Term where
  bimap f g (T h ts) = T (f h) (fmap (bimap f g) ts)
  bimap _ g (V x)    = V (g x)

instance Bifoldable Term where
  bifoldMap f g (T h ts) = f h `mappend` mconcat (fmap (bifoldMap f g) ts)
  bifoldMap _ g (V x)    = g x

type Occur = Int

vars' :: Ord b => Term a b -> M.Map b Occur
vars' (V x)    = M.singleton x 1
vars' (T _ ts) = M.unionsWith (+) (fmap vars' ts)

vars :: Ord b => Term a b -> S.Set b
vars = S.fromList . fmap fst . M.toList . vars'

occurVars :: Ord b => Occur -> Term a b -> [b]
occurVars n = map fst . filter ((==) n . snd) . M.toList . vars'

freeVars :: Ord b => Term a b -> [b]
freeVars = occurVars 1

boundVars :: Ord b => Term a b -> [b]
boundVars = occurVars 2

nestedVars :: Ord b => Term a b -> S.Set b
nestedVars (V _) = mempty
nestedVars (T _ xs) = mconcat (map vars xs)

type Arity = Int

isVar :: Term a b -> Bool
isVar (V _) = True
isVar _     = False

isTerm :: Term a b -> Bool
isTerm (T _ _) = True
isTerm _       = False

depth :: Term a b -> Int
depth (V _)    = 0
depth (T _ ts) = 1 + foldl' max 0 (map depth ts)

-- | Replace variables in terms with Ints.
rename :: (Bifunctor f, Bifoldable f, Ord v) => f a v -> f a Int
rename r = bimap id g r
  where
    vs  = S.toList $ bifoldMap mempty S.singleton r
    vs' = [1..] :: [Int]
    n   = M.fromList (zip vs vs')
    g x = let Just x' = M.lookup x n in x'

rename' :: (Bifunctor f, Ord b) => M.Map b c -> f a b -> f a c
rename' m = renameWith (\y -> let Just y' = M.lookup y m in y')

renameWith :: (Bifunctor f, Ord b) => (b -> c) -> f a b -> f a c
renameWith = bimap id

-- | Test if term is flat. A flat term is either a variable or a
--   function with only variables as arguments.
flat :: Term a b -> Bool
flat (V _)    = True
flat (T _ ts) = all isVar ts

-- | Return all subterms of a term.
subterms :: Term a b -> [Term a b]
subterms v@(V _)    = [v]
subterms t@(T _ xs) = t : concatMap subterms xs

arity :: Term a b -> Maybe (a, Int)
arity (V _)    = Nothing
arity (T f xs) = Just (f, length xs)

-- | Return arities of functions in given terms.
arities :: Ord a => [Term a b] -> Map a (S.Set Arity)
arities = M.unionsWith S.union . map (uncurry M.singleton . second S.singleton) . mapMaybe arity
