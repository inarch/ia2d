module Language.Pin.Program (
    Program(..)
  , renamingFrom
  , renaming
  ) where

import           Data.Bifoldable
import           Data.Map         (Map)
import qualified Data.Map         as M
import           Data.Monoid
import qualified Data.Set         as S

import           Language.Pin.Pin (Net (), Rule ())

data Program a b =
  Program {
    ruleSet :: [Rule a b]
  , nets    :: [Net a b]
  } deriving (Eq, Ord, Show)

instance Monoid (Program a b) where
  mempty = Program [] []
  mappend (Program rs1 ns1) (Program rs2 ns2) = Program (rs1 <> rs2) (ns1 <> ns2)

renamingFrom :: (Enum b, Bifoldable p, Ord k) => b -> [p a k] -> Map k b
renamingFrom from cs = M.fromList (zip vs [from..])
  where
    vs  = S.toList $ foldr (S.union . bifoldMap mempty S.singleton) mempty cs

renaming :: (Bifoldable p, Ord k) => [p a k] -> Map k Int
renaming = renamingFrom (1 :: Int)
