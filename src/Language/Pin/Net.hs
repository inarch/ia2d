module Language.Pin.Net (
    Net(..)
  , Comp(..)
  , compTerms
  ) where

import Data.Bifunctor
import Data.Bifoldable
import Data.Monoid ((<>))

import Language.Pin.Term

data Comp a b = Comp (Term a b) (Term a b) deriving (Eq, Ord, Show)

newtype Net a b
  = Net {
    components :: [Comp a b]
  }
  deriving (Eq, Ord, Show)

instance Bifunctor Comp where
  bimap f g (Comp t u) = Comp (bimap f g t) (bimap f g u)

instance Bifoldable Comp where
  bifoldMap f g (Comp t u) = bifoldMap f g t <> bifoldMap f g u

instance Monoid (Net a b) where
  mempty = Net []

  mappend (Net xs) (Net ys) = Net (xs ++ ys)

-- | Return all terms in net component
compTerms :: Comp a b -> [Term a b]
compTerms (Comp t u) = [t, u]
