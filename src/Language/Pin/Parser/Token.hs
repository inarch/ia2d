{-# LANGUAGE FlexibleContexts #-}

module Language.Pin.Parser.Token where

import           Control.Monad (void)
import           Data.List     (intercalate)
import           Text.Parsec

symbol :: Stream s m Char => String -> ParsecT s u m String
symbol s = string s <* spaces

comma :: Stream s m Char => ParsecT s u m String
comma = symbol ","

commaSep :: Stream s m Char => ParsecT s u m a -> ParsecT s u m [a]
commaSep p = p `sepBy` comma

commaSep1 :: Stream s m Char => ParsecT s u m a -> ParsecT s u m [a]
commaSep1 p = p `sepBy1` comma

semi :: Stream s m Char => ParsecT s u m String
semi = symbol ";"

parens :: Stream s m Char => ParsecT s u m a -> ParsecT s u m a
parens = between (symbol "(") (symbol ")")

brackets :: Stream s m Char => ParsecT s u m a -> ParsecT s u m a
brackets = between (symbol "[") (symbol "]")

escapedChar :: Stream s m Char => ParsecT s u m Char
escapedChar = do
  void $ char '\\'
  c <- anyChar
  case c of
    '\"' -> return '"'
    _    -> error $ "escapedChar: invalid escape sequence `\\" ++ [c] ++ "'"

literalString :: Stream s m Char => ParsecT s u m String
literalString = between (char '"') (char '"') $ many (try escapedChar <|> noneOf "\"")

spaceDelim :: Stream s m Char => ParsecT s u m ()
spaceDelim = skipMany1 . oneOf $ " \t"

qualifiedName :: Stream s m Char => ParsecT s u m String
qualifiedName = intercalate "." <$> many1 letter `sepBy` char '.'
