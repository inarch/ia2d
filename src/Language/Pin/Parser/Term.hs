{-# LANGUAGE OverloadedStrings #-}

module Language.Pin.Parser.Term (
    identifier
  , var
  , varSpecial
  , fun
  , term
  ) where

import           Language.Pin.Parser.Token
import           Language.Pin.Term

import           Data.Text                 (Text)
import qualified Data.Text                 as T
import           Text.Parsec
import           Text.Parsec.Text          (Parser)

identifier :: Parser Text
identifier = do
  let startChar = letter <|> char '_' <|> char '-'
  c  <- startChar
  cs <- many (startChar <|> digit)
  ps <- many $ char '\''
  spaces
  return . T.pack $ c : (cs ++ ps)

term :: Parser v -> Parser (Term Text v)
term p = try (V <$> p) <|> fun p

-- |Default parser for variables
var :: Parser Text
var = identifier <* notFollowedBy (char '(') <* spaces

varSpecial :: Parser Text
varSpecial = T.pack <$>
  many1 (letter <|> digit <|> oneOf "!@$%^&*_-=+'./")
    <* notFollowedBy (char '(')
    <* spaces

fun :: Parser v -> Parser (Term Text v)
fun p = do
  t <- identifier
  ts <- parens $ commaSep (term p)
  return $ T t ts
