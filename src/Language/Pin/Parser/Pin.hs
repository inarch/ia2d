{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}

module Language.Pin.Parser.Pin (
    TextRule
  , TextComp
  , program
  , program'
  , rule
  , net
  , netComponent
  , joinOp
  , import_
  ) where

import           Language.Pin.Parser.Term  (fun, term, var)
import           Language.Pin.Parser.Token
import           Language.Pin.Pin          (Net(..), Comp(..), Rule(..))

import           Control.Monad             (void)
import           Data.Either               (lefts, rights)
import           Data.Text                 (Text)
import           Text.Parsec
import           Text.Parsec.Text          (Parser)

type TextNet = Net Text Text

type TextComp = Comp Text Text

type TextRule = Rule Text Text

import_ :: Stream s m Char => ParsecT s u m FilePath
import_ = try (string "import") >> spaceDelim >> literalString

joinOp :: Stream s m Char => ParsecT s u m ()
joinOp = void $ symbol "><"

netComponent :: Parser v -> Parser (Comp Text v)
netComponent pv = do
  lhs <- term pv
  void $ symbol "~"
  rhs <- term pv
  return $ Comp lhs rhs

net :: Parser v -> Parser [Comp Text v]
net pv = commaSep (netComponent pv)

rule :: Parser v -> Parser (Rule Text v)
rule pv = do
  lhs <- fun pv
  joinOp
  rhs <- fun pv
  void $ symbol "=>"
  zs <- net pv
  return $ Rule lhs rhs (Net zs)

program' :: Parser v -> Parser ([FilePath], [Rule Text v], [Net Text v])
program' pv = do
  spaces
  imports <- endBy import_ endOfLine
  spaces
  parts <- flip endBy semi $ try (Right . Net <$> net pv) <|> (Left <$> rule pv)
  eof
  return (imports, lefts parts, rights parts)

program :: Parser ([TextRule], [TextNet])
program = program' var >>= \(_,b,c) -> return (b,c)
