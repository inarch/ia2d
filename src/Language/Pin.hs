{-# LANGUAGE OverloadedStrings, GeneralizedNewtypeDeriving, FlexibleContexts #-}

module Language.Pin (
    Program(..)
  , checkProg
  , loadProg
  , runWithInput
  , mainWithInput
  , evalLoad
  ) where

import           Args
import qualified Automaton                as A
import           Display.Pretty
import           Language.Pin.Error
import qualified Language.Pin.Parser.Pin  as PinP
import           Language.Pin.Parser.Term (var, varSpecial)
import qualified Language.Pin.Pin         as Pin
import           Language.Pin.Program
import qualified Language.Pin.Program     as P
import           Language.Pin.Valid       (checkProg)

import           Control.Monad.Except
import           Data.Foldable            (fold)
import           Data.Text                (Text)
import qualified Data.Text                as T
import qualified Data.Text.IO             as TIO
import           System.Directory         (Permissions (..), doesFileExist,
                                           getPermissions)
import           System.FilePath          (normalise, takeDirectory, (</>))
import           Text.Parsec              (parse)

newtype Load a r
  = Load (ExceptT (LoadError a) IO r)
  deriving (Functor, Applicative, Monad, MonadIO, MonadError (LoadError a))

evalLoad :: Load a r -> IO (Either (LoadError a) r)
evalLoad (Load f) = runExceptT f

-- * Loading

readProg' :: [String] -> FilePath -> Load Text (Program Text Text)
readProg' acc path = do
  let npath = normalise path
  let dpath = takeDirectory path

  when (npath `elem` acc) $ throwError . LoadError $ "Cyclic import of '" ++ path ++ "'."

  fileExists <- liftIO $ doesFileExist path
  unless fileExists $ throwError . LoadError $ "The file does not exist."

  isReadable <- liftIO $ readable `fmap` getPermissions path
  unless isReadable $ throwError . LoadError $ "The file is not readable."

  c <- liftIO (preprocess <$> TIO.readFile path)

  (options, _, _) <- liftIO getOptions
  let p = PinP.program' (if liberalVars options then varSpecial else var)

  case parse p path c of
    Right (ps, rs, ns) -> do
      checkProg rs ns
      let p = Program rs ns
      if null ps then
        return p
       else do
        let acc' = npath : acc
        ps' <- forM ps $ \importPath -> readProg' acc' (dpath </> importPath) `catchError` (throwError . ImportError importPath)
        let finalProg@(Program allRules allNets) = fold $ p : ps'
        checkProg allRules allNets -- arity mismatch may be produced from imports
        return finalProg
    Left e -> throwError . fromParseError (T.unpack c) $ e

loadProg' :: FilePath -> Load Text (Program Text Text)
loadProg' path = readProg' [] path `catchError` (throwError . FileError path)

-- | Load a program from a file.
loadProg :: FilePath -> IO (Either (LoadError Text) (P.Program Text Text))
loadProg = evalLoad . loadProg'

-- * Preprocessing and parsing

preprocess :: Text -> Text
preprocess = dropComments
  where
    dropComments = T.unlines . map (fst . T.breakOn "#") . T.lines

-- * Translation

runWithInput :: (Ord b, Show b, Pretty b) => Options -> P.Program Text b -> [Pin.Net Text b] -> IO ()
runWithInput opts p i = A.run opts $ p { nets = i }

mainWithInput :: (Ord b, Show b, Pretty b) => P.Program Text b -> [Pin.Net Text b] -> IO ()
mainWithInput p i = do
  (opts, _, errs) <- getOptions
  case errs of
    [] -> runWithInput opts p i
    _  -> mapM_ (\e -> putStrLn $ "[E] " ++ e) errs
