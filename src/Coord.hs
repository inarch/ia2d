module Coord (
    -- * Types
    Coord
  , RelCoord
  , AbsCoord
  , Scope
    -- * Operations
  , add
    -- * Predicates
  , scope
  , distance
    -- * Functions
  , sortDist
  , neighborhood
  ) where

import           Data.Function (on)
import qualified Data.List as L

type Coord = (Int, Int)
type RelCoord = Coord
type AbsCoord = Coord

type Scope = (Coord, Coord)

add :: Coord -> Coord -> Coord
add (x1, x2) (y1, y2) = (x1 + y1, x2 + y2)

distance :: Coord -> Coord -> Int
distance (x1, x2) (y1, y2) = abs (x1 - y1) + abs (x2 - y2)

scope :: Scope -> Coord -> Bool
scope ((xmin, ymin), (xmax, ymax)) (x, y) =
  xmin <= x && x <= xmax && ymin <= y && y <= ymax

sortDist :: AbsCoord -> [AbsCoord] -> [AbsCoord]
sortDist p = L.sortBy (compare `on` distance p)

neighborList :: [AbsCoord]
neighborList = (0, 0) :
  concat [
    concat [ [(c-x, x), (x-c, -x), (-x, c-x), (x, x-c)] | x <- [0 .. c - 1] ]
  | c <- [1 .. ] ]

-- | Distance-sorted neighboring coordinates.
neighborhood :: Int -> [RelCoord]
neighborhood d =
  take (1 + 2 * d * (d + 1)) neighborList
