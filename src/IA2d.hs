module IA2d (
    Term(..), Rule(..), Net(..), Comp(..),
    Options(),
    getOptions,
    defaultOptions,
    loadProg,
    runWithInput, mainWithInput,
    pretty,
  ) where

import Args
import Language.Pin
import Language.Pin.Term
import Language.Pin.Pin
import Display.Pretty
