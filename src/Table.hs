module Table ( Align(..)
             , showTable
             ) where

import Data.List (intercalate, transpose)

import Data.List.Helpers (for)

data Align = AlignLeft | AlignRight | AlignCenter

padLeft :: Int -> String -> String
padLeft w s | length s >= w = s
            | otherwise = replicate (w - length s) ' ' ++ s

padRight :: Int -> String -> String
padRight w s | length s >= w = s
             | otherwise = s ++ replicate (w - length s) ' '

center :: Int -> String -> String
center w s | length s >= w = s
           | otherwise     = let n = w - length s
                                 (x,y) = n `quotRem` 2
                                 l = replicate x       ' '
                                 r = replicate (x + y) ' '
                             in l ++ s ++ r

mapTable :: (a -> b) -> [[a]] -> [[b]]
mapTable = map . map

f :: [[String]] -> String
f = intercalate "\n" . map unwords

showTable :: (a -> String) -> [Align] -> [[a]] -> String
showTable p as rows = f out
  where rows' = mapTable p rows
        cols  = transpose rows'
        cols' = mapTable length cols
        widths = map maximum cols'
        afs = for as $ \x ->
                case x of
                  AlignLeft   -> padRight
                  AlignRight  -> padLeft
                  AlignCenter -> center
        afs' = afs ++ repeat padRight
        fs = zipWith ($) afs' widths
        out = transpose . map (uncurry map) $ zip fs cols
