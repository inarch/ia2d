module Display.Terminal (printGrid) where

import Data.Array

import Grid as G
import Display.Pretty
import Data.List.Helpers (for)
import Table
import qualified PotRedex as R

f :: Pretty a => Maybe (R.Cell a) -> [String]
f (Just r) = [pretty r ++ "   "]
f Nothing = [" - " ++ "   "]

printGrid :: Pretty a => G.Grid (R.Cell a) -> IO ()
printGrid g =
  let rows = for [i_min..i_max] $ \i ->
               for [j_min..j_max] $ \j ->
                 G.lookup g (i,j)
      ((i_min,j_min),(i_max,j_max)) = bounds g
  in do
    let rows' = map (concatMap f) rows
    let alignment = concat (repeat [AlignRight,AlignCenter,AlignLeft])
    let output = showTable id alignment rows'
    putStrLn output
