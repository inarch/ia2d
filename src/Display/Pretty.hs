module Display.Pretty (
    Pretty(..)
  ) where

import qualified Language.Pin.Pin      as Pin
import           Language.Pin.Term     (Term(..))
import           PotRedex              as R

import           Data.List             (intercalate)
import           Data.Text             (Text)
import qualified Data.Text             as T

class Pretty a where
  pretty' :: Bool -> a -> String

  pretty :: a -> String
  pretty = pretty' False

instance Pretty Int where
  pretty' _ = show

instance (Pretty a, Pretty b) => Pretty (a, b) where
  pretty' _ (x,y) = "(" ++ pretty x ++ "," ++ pretty y ++ ")"

instance Pretty Integer where
  pretty' _ = show

instance Pretty Char where
  pretty' _ c = [c]

instance Pretty Text where
  pretty' _ = T.unpack

-- Term

pot :: Pretty a => (a, Int) -> String
pot (u, 1) = pretty u
pot (u, n) = pretty u ++ "^" ++ show n

-- `pretty' True` outputs n nested applications of f as f^n(...)

instance (Eq a, Pretty a, Pretty b) => Pretty (Term a b) where
  pretty = prettyTerm True
  pretty' = prettyTerm

prettyTerm' :: (Eq a, Pretty a, Pretty b) => (a, Int) -> Term a b -> String
prettyTerm' (u, n) (T t [t1])
  | u == t    = let acc = (u, n + 1) in prettyTerm' acc t1
  | otherwise = let acc = (t, 1) in pot (u, n) ++ "(" ++ prettyTerm' acc t1 ++ ")"
prettyTerm' (u, n) t = pot (u, n) ++ "(" ++ prettyTerm True t ++ ")"

-- Pretty-print term.
-- Set the first argument to True to fold iteratively applied functions
-- (i.e. print s(s(s(z()))) as s^3(z())).
prettyTerm :: (Eq a, Pretty a, Pretty b) => Bool -> Term a b -> String
prettyTerm _ (V x)          = pretty x
prettyTerm True (T t [t1])  = prettyTerm' (t, 1) t1
prettyTerm foldPow (T t ts) =
  pretty t ++ "(" ++ intercalate "," (map (prettyTerm foldPow) ts) ++ ")"

-- Pin

instance (Eq a, Pretty a, Pretty b) => Pretty (Pin.Net a b) where
  pretty' p = intercalate ", " . map (pretty' p) . Pin.components

instance (Eq a, Pretty a, Pretty b) => Pretty (Pin.Comp a b) where
  pretty' p (Pin.Comp t u) = prettyTerm p t ++ "~" ++ prettyTerm p u

instance (Eq a, Pretty a, Pretty b) => Pretty (Pin.Rule a b) where
  pretty' p (Pin.Rule t u n) =
    prettyActivePair p t u ++ " => " ++ pretty' p n

instance (Eq a, Pretty a, Pretty b) => Pretty (Pin.NormalRule a b) where
  pretty' p (Pin.NormalRule t u n) =
    prettyActivePair p t u ++ " <= " ++ pretty' p n

prettyActivePair ::
  (Pretty a, Pretty b, Eq a)
    => Bool -> Term a b -> Term a b -> [Char]
prettyActivePair p t u = prettyTerm p t ++ " >< " ++ prettyTerm p u

-- Label

instance Pretty a => Pretty (R.Label a) where
  pretty Waiter = "-"
  pretty Forwarder = "&"
  pretty Delta = "delta"
  pretty Epsilon = "epsilon"
  pretty (Input _) = "in"
  pretty (Output _) = "out"
  pretty (Custom x) = pretty x

  pretty' _ = pretty

-- Cell

instance Pretty a => Pretty (R.Node a) where
  pretty' p (Node l r) = pretty' p l ++ "[" ++ intercalate "," (map (pretty' p) r) ++ "]"

-- PotRedex

instance Pretty a => Pretty (R.Cell a) where
  pretty' p (Cell c1 c2 _) = pretty' p c1 ++ " >< " ++ pretty' p c2
