module Display.Stats where

import qualified Automaton.Types       as A
import           Control.Monad.Helpers

import           Control.Monad         (forM_, when)
import           Data.Function         (on)
import           Data.IORef
import           Data.List             (intercalate, sortBy)
import           Data.Map              (Map)
import qualified Data.Map              as M
import           Data.Time.Clock
import           System.Console.ANSI
import           Text.Printf

withIORef :: IORef a -> (a -> IO b) -> IO b
withIORef r f = readIORef r >>= f

withIORefMaybe :: IORef (Maybe a) -> (a -> IO ()) -> IO ()
withIORefMaybe r f = do
  x <- readIORef r
  case x of
    Just y  -> f y
    Nothing -> return ()

put :: IORef (Maybe a) -> a -> IO ()
put r x = modifyIORef' r f
  where
    f (Just y) = Just y
    f Nothing  = Just x

overwrite :: IORef (Maybe a) -> a -> IO ()
overwrite r x = modifyIORef' r (const (Just x))

printStats :: A.MInput a -> Either String [String] -> A.RunState a b -> [A.MOutput] -> IO ()
printStats i r s xs = do
  fstTimestamp <- newIORef (Nothing :: Maybe DiffTime)
  lstTimestamp <- newIORef (Nothing :: Maybe DiffTime)
  parSteps <- newIORef (M.empty :: Map (Int,Int) Int)

  let partial = case r of Left _ -> " (partial run)"; Right _ -> ""

  putStrLn $ "SEQ STEPS: " ++ show (A.seqSteps s) ++ partial
  putStrLn $ "PAR STEPS: " ++ show (A.parSteps s) ++ partial
  whenRight r $ const $ do
    let speedup = fromIntegral (A.seqSteps s) / fromIntegral (A.parSteps s) :: Float
    putStrLn $ "SPEEDUP: " ++ printf "%.2f" speedup

  forM_ xs $ \x ->
    case x of
      A.ParStep nSucc nFail ->
        modifyIORef' parSteps $ M.insertWith (+) (nSucc, nFail) 1
      A.Timestamp t -> do
        put fstTimestamp t
        overwrite lstTimestamp t
      _ -> return ()

  when (A.verbose i) $ do
    putStrLn "PASSES BREAKDOWN:"
    let f ((fired, delayed), n) = putStrLn $ "  " ++ show n ++ " x " ++ show fired ++ " fired" ++ (if delayed > 0 then " and " ++ show delayed ++ " delayed" else "")
    withIORef parSteps $ mapM_ f . sortBy (compare `on` (fst . fst)) . M.toList

  withIORefMaybe fstTimestamp $ \t0 ->
    withIORefMaybe lstTimestamp $ \tf ->
      putStrLn $ "DURATION:\n  " ++ show (tf - t0)

  case r of
    Left m -> do
      setSGR [SetColor Foreground Vivid Red]
      putStrLn $ "ABORT: " ++ m
      setSGR [Reset]
    Right ol | not (A.keepOutput i) ->
      putStrLn $ "OUTPUT:\n  " ++ intercalate "\n  " ol
    Right _ ->
      putStrLn "NO OUTPUT (--keep)"
