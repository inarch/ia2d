{-# LANGUAGE OverloadedStrings #-}

module Display.SVG where

import Data.Text (Text)
import Data.Maybe (catMaybes)
import qualified Data.Text as T
import qualified Data.Text.Lazy.IO as TIO
import Text.Printf
import Lucid.Svg
import Data.List (intercalate)
import Display.Pretty
import Data.Array (assocs)
import qualified Grid as G
import qualified PotRedex as R

data Colors = Colors {
    fontColor :: Text
  , boxColor :: Text
  , arrowColor :: Text
  , markerId :: Text
  } deriving Show

type M a = Svg a

style :: String
style =
  "text { font-size: 32; text-anchor: middle; } " ++
  "text.lab-io { stroke: darkblue; } " ++
  "text.lab-r { stroke: darkred; } " ++
  "text.lab-a { stroke: black; } " ++
  "text.lab-i { stroke: black; } " ++
  ".box-f { stroke: gray; fill: none; } " ++
  ".box-io { stroke: blue; fill: lightgray; fill-opacity: 0.25; } " ++
  ".box-r { stroke: red; fill: lightgray; fill-opacity: 0.25; } " ++
  ".box-a { stroke: black; fill: lightgray; fill-opacity: 0.25; } " ++
  ".box-i { stroke: gray; fill: lightgray; fill-opacity: 0.25; } " ++
  ".arr-io { stroke: gray; fill: none; marker-end: url(#Arrow); } " ++
  ".arr-r { stroke: darkred; fill: none; marker-end: url(#Arrow_r); } " ++
  ".arr-a { stroke: gray; fill: none; marker-end: url(#Arrow); } " ++
  ".arr-i { stroke: gray; fill: none; marker-end: url(#Arrow); } "

markerColors :: [(Text, Text)]
markerColors = [("Arrow_r", "darkred"), ("Arrowg", "gray")]

styleId :: G.Grid (R.Cell a) -> (Int, Int) -> String
styleId g p =
  case G.lookup g p of
    Nothing -> "f"
    Just c | R.isNormalRedex c -> "r"
    Just c | R.isIORedex c -> "io"
    Just c | R.isOccupied c -> "a"
    Just _ -> "i"

t :: Int -> Text
t = T.pack . show

_0, boxWidth, boxHeight, spacing :: Int
boxWidth = 80
boxHeight = 80
spacing = 40
_0 = 0

fx, fy :: Int -> Int
fx j = j*(boxWidth + spacing) - boxWidth `quot` 2
fy i = i*(boxHeight + spacing) - boxHeight `quot` 2

data BoxHPos = BoxLeft | BoxLeftHalf | BoxCenter | BoxRightHalf | BoxRight deriving (Eq, Ord, Show)
data BoxVPos = BoxTop | BoxTopHalf | BoxMiddle | BoxBottomHalf | BoxBottom deriving (Eq, Ord, Show)

pos :: BoxHPos -> BoxVPos -> (Int, Int) -> (Int, Int)
pos hpos vpos (i, j) = (fx j + dx, fy i + dy)
  where
    dx =  case hpos of
            BoxLeft      -> 0
            BoxLeftHalf  -> boxWidth `quot` 4
            BoxCenter    -> boxWidth `quot` 2
            BoxRightHalf -> 3*(boxWidth `quot` 4)
            BoxRight     -> boxWidth
    dy =  case vpos of
            BoxTop        -> 0
            BoxTopHalf    -> boxHeight `quot` 4
            BoxMiddle     -> boxHeight `quot` 2
            BoxBottomHalf -> 3*(boxHeight `quot` 4)
            BoxBottom     -> boxHeight

dashing :: Int -> Text
dashing n = T.pack . intercalate "," . map show $ xs
  where
    xs = 30 : 10 : take (2*n) (cycle [5, 10]) :: [Int]

box :: String -> (Int, Int) -> Maybe Text -> Maybe Text -> M ()
box st (i,j) a b =
  do
    rect
    sep
    sequence_ (catMaybes [label1 <$> a, label2 <$> b])
  where
    x = fx j
    y = fy i
    font_size = 32
    rect = rect_ [ x_ . t $ x
                 , y_ . t $ y
                 , width_ . t $ boxWidth
                 , height_ . t $ boxHeight
                 , class_ (T.pack ("box-" ++ st))
                 ]
    sep = line_ [ x1_ . t $ x
                , x2_ . t $ x + boxWidth
                , y1_ . t $ y + boxHeight `quot` 2
                , y2_ . t $ y + boxHeight `quot` 2
                , class_ (T.pack ("box-" ++ st))
                ]
    label1, label2 :: Text -> M ()
    label1 text
           = text_ [ x_ . t $ x + boxWidth `quot` 2
                   , y_ . t $ y + boxHeight `quot` 4 + font_size `quot` 4
                   , class_ (T.pack (if text == "in" || text == "out" then "lab-io" else "lab-" ++ st))
                   ] $ toHtml text
    label2 text
           = text_ [ x_ . t $ x + boxWidth `quot` 2
                   , y_ . t $ y + 3*(boxHeight `quot` 4) + font_size `quot` 4
                   , class_ (T.pack (if text == "in" || text == "out" then "lab-io" else "lab-" ++ st))
                   ] $ toHtml text

loop :: Int -> (Int, Int) -> Bool -> String -> M ()
loop r (x, y) bottom c =
    path_ [ d_ $ T.pack (printf "M%d,%d a%d,%d 0 1,%d %d,%d" x y r r f r r)
          , class_ (T.pack ("arr-" ++ c))
          --, stroke_dasharray_ $ dashing aux
          ]
  where
    f = if bottom then _0 else 1

varr :: Int -> (Int, Int) -> Int -> String -> M ()
varr d (x,y1) y2 c =
  path_ [ d_ $ T.pack (printf "M%d,%d q%d,%d %d,%d" x y1 kx ky qx qy)
        , class_ (T.pack ("arr-" ++ c))
        ]
  where
    (qx, qy) = (_0, negate $ y1 - y2)
    (kx, ky) = (d, qy `quot` 2)

harr :: Int -> (Int, Int) -> Int -> String -> M ()
harr d (x1,y) x2 c =
  path_ [ d_ $ T.pack (printf "M%d,%d q%d,%d %d,%d" x1 y kx ky qx qy)
        , class_ (T.pack ("arr-" ++ c))
        ]
  where
    (qx, qy) = (negate $ x1 - x2, _0)
    (kx, ky) = (qx `quot` 2, d)

darr :: (Int, Int) -> (Int, Int) -> String -> M ()
darr (x1,y1) (x2,y2) c =
  line_ [ x1_ . t $ x1
        , y1_ . t $ y1
        , x2_ . t $ x2
        , y2_ . t $ y2
        , class_ (T.pack ("arr-" ++ c))
        -- , stroke_dasharray_ $ dashing 2
        ]

arr :: String -> (Int, Int, Int) -> (Int, Int) -> M ()
arr st (i,j,p) (k,l) =
  case (i == k, j == l) of
    -- diagonal arrow
    (False, False) ->
      let (h1, h2) = if j < l then (BoxRight, BoxLeft) else (BoxLeft, BoxRight)
          (v1, v2) = (if p == 0 then BoxTopHalf else BoxBottomHalf, BoxMiddle)
      in darr (pos h1 v1 (i,j)) (pos h2 v2 (k,l)) st
    -- loop
    (True, True) ->
      let (h,v) = if p == 0 then (BoxRightHalf, BoxTop) else (BoxLeft, BoxBottomHalf)
      in loop (spacing `quot` 2) (pos h v (i,j)) (p /= 0) st
    -- horizontal arrow
    (True, _) ->
      let b = if p == 0 then -spacing else spacing
          dx = boxWidth `quot` 2
          dy = if p == 0 then 0 else boxHeight
      in harr b (fx j + dx, fy i + dy) (fx l + dx) st
    -- vertical arrow
    (_, True) ->
      let (d, dx) = if p == 0 then (-spacing, 0) else (spacing, boxWidth)
          dy = if p == 0 then boxHeight `quot` 4 else 3*(boxHeight `quot` 4)
      in varr d (fx j + dx, fy i + dy) (fy k + quot boxHeight 2) st

marker :: Text -> Text -> M ()
marker mid fillColor =
  marker_ [ id_ mid
          , viewBox_ "0 0 20 20"
          , refX_ "20"
          , refY_ "10"
          , markerUnits_ "strokeWidth"
          , markerWidth_ "10"
          , markerHeight_ "8"
          , orient_ "auto"
          , fill_ fillColor
          , term "z-index" (t 1)
          ] $ path_ [ d_ "M 0 0 L 20 10 L 0 20 z" ]

svg :: (Int, Int) -> M () -> M ()
svg (h, w) body =
    doctype_ >> with (svg11_ content) attributes
  where
    attributes = [width_ . t $ (w + 1)*(boxWidth + spacing), height_ . t $ (h + 1)*(boxHeight + spacing)]
    content = do
      termWith "style" [ type_ "text/css" ] . toHtmlRaw $ "<![CDATA[" ++ style ++ "]]>"
      defs_ $ mapM_ (uncurry marker) markerColors
      rect_ [width_ "100%", height_ "100%", fill_ "white"]
      body

toSVG :: (Show a, Pretty a) => G.Grid (R.Cell a) -> M ()
toSVG g =
  do
    mapM_ (uncurry h) $ assocs g
    mapM_ (uncurry f) $ assocs g
  where
    f p (Just (R.Cell c1 c2 _)) = box (styleId g p) p (dolabel c1) (dolabel c2)
    f p Nothing = box (styleId g p) p Nothing Nothing
    dolabel = Just . T.pack . pretty . R.label
    h p (Just (R.Cell c1 c2 _)) = do
      mapM_ (arrows p 0) $ R.coords c1
      mapM_ (arrows p 1) $ R.coords c2
    h _ Nothing = return ()
    arrows (i,j) w (k,l) = arr (styleId g (i, j)) (i,j,w) (k,l)

writeSVG :: (Show a, Pretty a) => FilePath -> G.Grid (R.Cell a) -> IO ()
writeSVG path g =
  TIO.writeFile path $ renderText (svg (G.size g) (toSVG g))

