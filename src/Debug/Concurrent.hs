module Debug.Concurrent (
    MVar
  , newEmptyMVar
  , newMVar
  , putMVar
  , readMVar
  , takeMVar
  , tryPutMVar
  , tryReadMVar
  , tryTakeMVar
  , tracePutMVar
  , traceTakeMVar
  , traceReadMVar
  , forkIO
  , traceM
  ) where

import Debug.Trace
import Control.Monad (unless)
import Control.Concurrent (forkIO, MVar, newEmptyMVar, newMVar, tryTakeMVar, tryPutMVar, tryReadMVar)
import qualified Control.Concurrent.MVar as MVar

traceTakeMVar :: String -> MVar a -> IO a
traceTakeMVar msg v = do
  mx <- tryTakeMVar v
  case mx of
    Just x -> return x
    Nothing -> traceM msg >> MVar.takeMVar v

takeMVar :: MVar a -> IO a
takeMVar = traceTakeMVar "takeMVar: blocked"

tracePutMVar :: String -> MVar a -> a -> IO ()
tracePutMVar msg v x = do
  p <- tryPutMVar v x
  unless p (traceM msg >> MVar.putMVar v x)

putMVar :: MVar a -> a -> IO ()
putMVar = tracePutMVar "putMVar: blocked"

traceReadMVar :: String -> MVar a -> IO a
traceReadMVar msg v = do
  mx <- tryReadMVar v
  case mx of
    Just x -> return x
    Nothing -> traceM msg >> MVar.readMVar v

readMVar :: MVar a -> IO a
readMVar = traceReadMVar "readMVar: blocked"
