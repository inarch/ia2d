{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Automaton.IO.Types (
    IORuleM
  , IORuleInput(..)
  , IORuleState(..)
  , NodeFuture(..)
  , runIORule
  ) where

import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Except

import Automaton.IO.State
import Grid as G
import Coord
import PotRedex as R
import Automaton.Error

-- * Data types

data NodeFuture a = Keep | Replace (Node a) deriving (Eq, Show)

data IORuleInput a =
  IORuleInput {
    maxDist :: Int
  , bound :: [Int]
  }

data IORuleState a =
  IORuleState {
    inputState :: InputState a Int
  , outputState :: OutputState a Int
  , grid :: G.Grid (R.Cell a)
  , space :: [AbsCoord]
  , count :: Bool
  }

-- The IORuleM monad is used as temporary environment for IO rules. If
-- the rule fails, then state changes are discarded, if the rule succeeds
-- the changes are commited to the global state.

newtype IORuleM a r = IORuleM {
    unIORuleM :: ExceptT (Error a) (ReaderT (IORuleInput a) (State (IORuleState a))) r
  }
  deriving (
    Functor
  , Applicative
  , Monad
  , MonadState (IORuleState a)
  , MonadError (Error a)
  , MonadReader (IORuleInput a)
  )

runIORule :: IORuleInput a -> IORuleState a -> IORuleM a r -> Either (Error a) (r, IORuleState a)
runIORule i s (IORuleM f) =
  let (e, s') = runState (runReaderT (runExceptT f) i) s in
  case e of
    Left e' -> Left e'
    Right r -> Right (r, s')
