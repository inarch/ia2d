module Automaton.IO.State (
    InputState
  , InputStateEntry(..)
  , OutputState
  , readOutput
  , nextKey
  ) where

import Data.Map (Map)
import qualified Data.Map as M

import Language.Pin.Term
import PotRedex

-- * Input

type InputState a b = Map Int (InputStateEntry a b)

data InputStateEntry a b
  = InputTerm (Term (Label a) b)
  | Connected -- ^ connected input in same IO pass, skip
  deriving (Show, Eq)

-- * Output

type OutputState a b = Map Int (Term (Label a) b)

readOutput :: OutputState a Int -> Int -> Term (Label a) Int
readOutput m x =
  case M.lookup x m of
    Just (V y)    -> V y
    Just (T f xs) -> T f (map (\(V y) -> readOutput m y) xs)

nextKey :: M.Map Int v -> Int
nextKey m = if M.null m then 0 else succ . fst . M.findMax $ m
