module Automaton.IO (
  forwardIORule
  ) where

import Control.Monad.State
import qualified Grid as G
import qualified PotRedex as R
import Automaton.Types (Redex)
import Automaton.IO.Types

-- * io rules

forwardIORule :: Redex a -> IORuleM a ()
forwardIORule (p, (R.Node R.Forwarder [q], node)) =
  modify $ \s -> s { grid = G.accumulate R.feed (G.free (grid s) p) [(q, node)] }
forwardIORule _ = error "forwardIORule"
