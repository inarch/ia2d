module Automaton.Rewrite (genRule) where

import           Prelude hiding (lookup)
import           Control.Monad (forM, when, unless)
import           Control.Monad.State (State, runState)
import           Control.Monad.Except (ExceptT, runExceptT, throwError)
import           Data.Function (on)
import qualified Data.Map as M
import qualified Data.List as L
import           Debug.Trace

import           Automaton.Types
import           Automaton.Error
import qualified PotRedex as R
import           Coord (AbsCoord, distance)
import qualified Grid as G
import           Language.Pin.Term (Term(..))
import qualified Language.Pin.Pin as P

debugOutput :: Bool
debugOutput = False

debug :: Monad m => String -> m ()
debug m = when debugOutput $ traceM ("[Input.Write] " ++ m)

data WriteState a b = WriteState {
    space :: [AbsCoord]
  , grid :: G.Grid (R.Cell a)
  , wsMaxDist :: Int
  , nRedexes :: Int
  }
  deriving Show

-- a: node labels, v: variable indentifiers, r: return value
type S a v r = ExceptT (Error a) (State (WriteState a v)) r

genRule
  :: (Ord a, Eq a, Show a)
    => Int                          -- ^ link range
    -> P.NormalRule (R.Label a) Int -- ^ rule
    -> G.Grid (R.Cell a)            -- ^ grid
    -> [AbsCoord]                   -- ^ allocable space
    -> Redex a                      -- ^ redex
    -> Either (Error a) (G.Grid (R.Cell a), Int)
genRule maxDist r g sp (pr, (a, b)) =
  let P.NormalRule (T l1 st1) (T l2 st2) (P.Net tl) = r
      (r1, r2) = if l1 == R.label a && l2 == R.label b
        then (R.coords a, R.coords b) else (R.coords b, R.coords a)
      (spr, spo) = splitAt (length tl) sp
      -- todo: we could get rid of the monad
      s = WriteState {
          space       = spo
        , grid        = foldr (\p g' -> G.allocate g' p R.empty) g spr
        , wsMaxDist   = maxDist
        , nRedexes    = 0
        }
      f (p, V v) = M.singleton v [p]; f _ = M.empty
      (e, s') = (flip runState s . runExceptT) $ do
        debug $ "reduce " ++ show a ++ " " ++ show b ++ " at " ++ show pr ++ " rule " ++ show r
        let l = zip r1 st1 ++ zip r2 st2
            m0 = M.unionsWith (++) (map f l)
            l' = concatMap (\(p, P.Comp t1 t2) -> [(p, t1), (p, t2)]) (zip spr tl)
            m = M.unionsWith (++) (map locate (l ++ l'))
        m' <- M.traverseWithKey (\k ps ->
          case M.lookup k m0 of
            Just [x] -> do
              modify $ \st -> st { grid = G.update (grid st) x R.soil }
              return x;
            _ -> alloc [] ps) m
        _ <- forM (l ++ l') $ \(p, t) -> write m' (Left p, t)
        debug $ "done " ++ show pr
  in if length sp < length tl then Left Delay else
    case e of
    Left err -> Left err
    Right () -> Right (grid s', nRedexes s')

-- | Allocate a location close to lists of locations (strictly and loosely).
alloc :: [AbsCoord] -> [AbsCoord] -> S a v AbsCoord
alloc l0 l = do
  sp <- space <$> get
  md <- wsMaxDist <$> get
  let fsp = filter (\p -> all (\p' -> distance p p' <= md) l0) sp
  when (null fsp) $ throwError Delay
  let f p = sum (map (\p' -> distance p p' * distance p p') l)
  let p = L.minimumBy (compare `on` f) fsp
  debug $ "selected " ++ show p ++ " around " ++ show l
  modify $ \st -> st {
      space = L.delete p (space st)
    , grid = G.allocate (grid st) p R.empty
  }
  return p

feed :: AbsCoord -> R.Node a -> S a Int ()
feed p n = do
  let u g = G.update g p (flip R.feed n)
  let f g = if R.isRedex (G.access g p) then succ else id
  modify $ \s -> let g' = u (grid s) in
    s { grid = g', nRedexes = f g' (nRedexes s) }

-- | Bring p within range of pt, possibly with one or more extension forwarders.
reach :: Show a => AbsCoord -> AbsCoord -> S a Int AbsCoord
reach pt p = do
  md <- wsMaxDist <$> get
  if p == pt || distance p pt <= md then return p else do
    pg <- alloc [pt] [p]
    debug $ "extension from " ++ show p ++ " to " ++ show pt ++ " via " ++ show pg
    p' <- reach pg p
    feed p' $ R.forward pg
    return pg

-- | Connect p to pt, with possibly one or more extension forwarders.
connect :: Show a => AbsCoord -> AbsCoord -> S a Int ()
connect p pt = do
  md <- wsMaxDist <$> get
  unless (p == pt) $
    if distance p pt <= md then feed p (R.forward pt) else do
      ph <- alloc [pt] [p]
      debug $ "connect from " ++ show p ++ " to " ++ show pt ++ " via " ++ show ph
      feed ph $ R.forward pt
      connect p ph

locate :: Show a => (AbsCoord, Term (R.Label a) Int) -> M.Map Int [AbsCoord]
locate (p, T _ xs) = M.unionsWith (++) (map (\t -> locate (p,t)) xs)
locate (p, V v) = M.singleton v [p]

-- | Write data onto the grid.
write :: Show a => M.Map Int AbsCoord  -> (Either AbsCoord AbsCoord, Term (R.Label a) Int) -> S a Int AbsCoord
write m (Left p, t@(T f xs)) = do
  debug $ "node " ++ show t ++ " at " ++ show p
  aux <- forM xs $ \st -> write m (Right p, st) >>= reach p
  feed p $ R.node f aux
  return p
write m (Right p, t@(T f xs)) = do
  debug $ "node " ++ show t ++ " from " ++ show p
  aux <- forM xs $ \st -> write m (Right p, st)
  p' <- alloc [p] aux
  aux' <- forM aux (reach p')
  feed p' $ R.node f aux'
  return p'
write m (Left p, t@(V v)) = do
  debug $ "link " ++ show t ++ " at " ++ show p
  connect p (m M.! v)
  return p
write m (Right p, t@(V v)) = do
  debug $ "link " ++ show t ++ " from " ++ show p
  return (m M.! v)
