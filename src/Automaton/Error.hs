module Automaton.Error where

data Error a =
    Delay          -- ^ delayed redex
  | NoInput        -- ^ no input (yet) available
  | Abort String   -- f Fatal error
  deriving (Eq, Show)

fatal :: Error a -> Maybe (Error a)
fatal (Abort e) = Just (Abort e)
fatal _ = Nothing

