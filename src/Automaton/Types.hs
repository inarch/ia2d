{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE Rank2Types                 #-}
{-# LANGUAGE RecordWildCards            #-}

module Automaton.Types (
    RunState(..)

  , IA()
  , MInput(..)
  , showMInput
  , MOutput(..)
  , runIA
  , put
  , get
  , modify
  , tell
  , ask
  , asks
  , miMaxDist

  , withRandomGen
  , incrParSteps
  , withParSteps
  , incrSeqSteps
  , withGrid

  , executeIA

  , Redex
  , RuleSet
  , RuleSyms
  , ruleSyms
  ) where

import           Automaton.Error
import           Automaton.IO.State          (InputState, OutputState)
import           Automaton.Migration.Options
import           Coord
import           Grid                        (Grid)
import qualified Language.Pin.Pin            as P
import           PotRedex

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Control.Monad.Writer.Strict
import           Data.List                   (sort)
import           Data.Map                    (Map)
import           Data.Time.Clock
import           System.Random

type Redex a = (AbsCoord, (Node a, Node a))

type RuleSyms a = (Label a, Label a)

ruleSyms :: Ord a => Label a -> Label a -> RuleSyms a
ruleSyms x y = let [x', y'] = sort [x, y] in (x', y')

type RuleSet a = Map (RuleSyms a) (P.NormalRule (Label a) Int)

data MInput a = MInput {
    migrationOpts :: MigrationOptions
  , miMaxSteps    :: Maybe Int
  , ruleset       :: RuleSet a
  , writeToStdout :: Bool
  , svgPath       :: Maybe FilePath
  , gridSize      :: (Int, Int)
  , initGen       :: Maybe StdGen
  , verbose       :: Bool
  , feedFlag      :: Bool
  , keepOutput    :: Bool
  , liberalVars   :: Bool
  , enterRepl     :: Bool
  , skipContrPass :: Bool
  }

showMInput :: MInput a -> String
showMInput MInput{..} =
  unlines
    [ "migrationOptions = " ++ show migrationOpts
    , "miMaxSteps = " ++ show miMaxSteps
    , "ruleset = <..>"
    , "writeToStdout = " ++ show writeToStdout
    , "svgPath = " ++ show svgPath
    , "gridSize = " ++ show gridSize
    , "initGen = " ++ show initGen
    , "verbose = " ++ show verbose
    , "feedFlag = " ++ show feedFlag
    , "keepOutput = " ++ show keepOutput
    , "liberalVars = " ++ show liberalVars
    , "enterRepl = " ++ show enterRepl
    , "skipContrPass = " ++ show skipContrPass
    ]

miMaxDist :: MInput a -> Int
miMaxDist MInput{..} = maxDist migrationOpts

data RunState a g = RunState {
    _rcRnd      :: g
  , parSteps    :: Int
  , seqSteps    :: Int
  , failedSteps :: Int
  , _rcGrid     :: !(Grid (Cell a))
  , outputState :: OutputState a Int
  , inputState  :: InputState a Int
  , mappings    :: [Map Int String]
  , outputs     :: [Int]
  , bound       :: [Int]
  , feedInput   :: Bool
  }

data MOutput
  = ParStep Int Int
  | Timestamp DiffTime
    deriving Show

newtype IA a g r = IA {
    runIA :: ExceptT (Error a) (ReaderT (MInput a) (StateT (RunState a g) (WriterT [MOutput] IO))) r
  } deriving (Functor, Applicative, Monad, MonadReader (MInput a), MonadState (RunState a g), MonadWriter [MOutput], MonadError (Error a), MonadIO)

executeIA :: MInput a -> RunState a g -> IA a g r -> IO (Either (Error a) r, RunState a g, [MOutput])
executeIA i s (IA f) =
  runWriterT (runStateT (runReaderT (runExceptT f) i) s) >>=
    \((e, s'), w) -> return (e, s', w)

withRandomGen :: RandomGen g => (g -> (b, g)) -> IA a g b
withRandomGen f = do
  g <- _rcRnd <$> get
  let (x, g') = f g
  modify $ \s -> s { _rcRnd = g' }
  return x

incrSeqSteps :: IA a g ()
incrSeqSteps = modify $ \s -> s { seqSteps = succ (seqSteps s) }

incrParSteps :: IA a g ()
incrParSteps = modify $ \s -> s { parSteps = succ (parSteps s) }

withParSteps :: (Int -> IA a g r) -> IA a g r
withParSteps f = (parSteps <$> get) >>= f

withGrid :: (Grid (Cell a) -> r) -> IA a g r
withGrid f = do
  g <- _rcGrid <$> get
  return (f g)
