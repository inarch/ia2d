module Automaton.Input (inputXRule, inputOutputRule, nextKey) where

import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Except
import Data.Maybe (mapMaybe)
import Data.List (sortBy, (\\), sort)
import Data.Function (on)

import Data.List.Helpers
import Automaton.IO.State
import Automaton.IO.Types
import Language.Pin.Term
import qualified Data.Map as M
import qualified Grid as G
import Coord
import qualified PotRedex as R
import Automaton.Types (Redex)
import Automaton.Error
import Display.Pretty

build
  :: (Eq a, Pretty a, Show a) =>
    R.Tag
    -> R.Label a
    -> [Term (R.Label a) Int]
    -> IORuleM a (NodeFuture a)
build tag l xs = do
  sp <- space <$> get
  m <- inputState <$> get
  let a = length xs -- arity
  when (lengthLt a sp) (throwError Delay)
  let keys = take a [(nextKey m)..]
  let entries = zipWith (\k t -> (k, InputTerm t)) keys xs
  modify $ \s ->
    s { inputState = M.delete tag (m `M.union` M.fromList entries)
      , grid = foldr (\(p', k) g -> G.allocate g p' (R.cell (R.input k) R.wait)) (grid s) (zip sp keys)
      , space = space s \\ take a sp
      }
  return $ Replace (R.node l (take a sp))

-- | Find position of link (other PotRedex that is tagged with same variable)
linkPos :: Int -> Int -> IORuleM a (Maybe (AbsCoord, Int))
linkPos tag x = do
  inTbl <- inputState <$> get
  case filter p . M.assocs $ inTbl of
    [] -> return Nothing
    (k, _) : _ -> do
      g <- grid <$> get
      case mapMaybe (\(q, r) -> if k `elem` R.allTags r then Just q else Nothing) (G.allocated g) of
        l : _ -> return (Just (l, k))
        []    -> return Nothing
  where
    p (k, entry) | InputTerm (V y) <- entry, k /= tag && x == y = True
                 | otherwise                                    = False

-- | Replace cell with certain tag in given PotRedex
replaceTagged :: Show a => R.Tag -> R.Node a -> R.Cell a -> R.Cell a
replaceTagged t n (R.Cell n1 n2 _)
  | R.tag n1 == Just t = R.cell n n2
  | R.tag n2 == Just t = R.cell n1 n
  | otherwise          = error $ "replaceTagged: no cell with tag " ++ show t

connect
  :: (Eq a, Pretty a, Show a) =>
    AbsCoord
    -> R.Tag
    -> Int -- variable
    -> IORuleM a (NodeFuture a)
connect p tag v = do
  md <- asks maxDist
  sp <- space <$> get
  g <- grid <$> get
  other <- linkPos tag v
  case other of
    Nothing ->
      throwError NoInput
    Just (q, otherTag) ->
      case sortBy (compare `on` distance q) sp of
        x : _
          -- direct link
          | distance x q <= md -> do
            let g2 = G.update g q (replaceTagged otherTag (R.forward x))
            let g' = G.allocate g2 x R.empty
            modify $ \s -> s { inputState = M.insert otherTag Connected (M.delete tag (inputState s))
                             , grid = g'
                             , space = space s \\ [x] }
            return $ Replace (R.forward x)
          -- move closer
          | distance x q < distance p q -> do
            let g' = G.allocate g x (R.cell (R.input tag) R.wait)
            modify $ \s -> s { inputState = M.insert otherTag Connected (inputState s)
                             , grid = g'
                             , space = space s \\ [x]
                             , count = False
                             }
            return $ Replace (R.forward x)
        -- no better space available
        _ -> throwError NoInput

expandNode :: (Eq a, Show a, Pretty a) => AbsCoord -> R.Node a -> IORuleM a (NodeFuture a)
expandNode p (R.Node (R.Input t) _) = do
  m <- inputState <$> get
  case M.lookup t m of
    Just (InputTerm (T l xs)) ->
      build t l xs
    Just (InputTerm (V v)) ->
      connect p t v
    Just Connected -> do
      modify $ \s -> s { inputState = M.delete t (inputState s) }
      return Keep
    _ -> error $ "no entry for tag " ++ show t
expandNode _ _ = return Keep

-- TODO: reordering here is not pretty
orderCell :: Ord a => R.Cell a -> R.Cell a
orderCell (R.Cell x1 x2 d) = R.Cell y1 y2 d
  where
    [y1, y2] = sort [x1, x2]

inputXRule :: (Eq a, Ord a, Show a, Pretty a) => Redex a -> IORuleM a ()
inputXRule (p, (n1@(R.Node (R.Input _) _), n2)) = do
  -- original redex
  R.Cell o1 o2 _ <- (orderCell . (\s -> G.access (grid s) p)) <$> get
  -- futures
  let future orig repl = case repl of Replace n -> n; Keep -> orig
  n1' <- future o1 <$> expandNode p n1
  n2' <- future o2 <$> expandNode p n2
  modify $ \s -> s { grid = G.allocate (G.free (grid s) p) p (R.cell n1' n2') }
inputXRule _ = error "input-X rule"

inputOutputRule :: (Eq a, Ord a, Show a, Pretty a) => Redex a -> IORuleM a ()
inputOutputRule (p, (input@(R.Node (R.Input tx) _), output@(R.Node (R.Output ty) _))) = do
  m <- inputState <$> get
  bvars <- asks bound
  case M.lookup tx m of
    Just (InputTerm (V x)) | x `notElem` bvars ->
      modify $ \s -> s { outputState = M.insert ty (V x) (outputState s)
                       , inputState = M.delete tx (inputState s)
                       , grid = G.free (grid s) p }
    _ -> inputXRule (p, (input, output))
inputOutputRule _ = error "input-output rule"
