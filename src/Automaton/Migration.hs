{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}

module Automaton.Migration (
    module Automaton.Migration.Options
  , migrate
  ) where

import           Automaton.Migration.Options
import           Automaton.Types
import           Coord
import           Data.List.Shuffle           (shuffle)
import           Data.Maybe
import           Display.Pretty
import qualified Grid                        as G
import qualified PotRedex                    as R
import           System.Random               (RandomGen)

import           Data.Function               (on)
import qualified Data.List                   as L

-- | Incentive to move to a target location.
incentive ::
     AbsCoord                 -- ^ target location
  -> MigrationOptions
  -> [AbsCoord]               -- ^ inbound locations
  -> [AbsCoord]               -- ^ outbound locations
  -> [AbsCoord]               -- ^ free neighbor locations
  -> Float
incentive p opts@MigrationOptions{..} ilinks olinks fneigh =
    b * (diffusion / fromIntegral spreadDist) - a * (tension / fromIntegral maxDist)
  where
    a = fromIntegral . sum . map (square . distance p) $ ilinks ++ olinks
    b = fromIntegral . sum . map (square . trunc . distance p) $ fneigh

    square x = x*x

    trunc x = max 0 (1 + spreadDist - x)

-- | Choose an intended move for a specific location.
-- Returns ((o,t), d) where o is the source, t is the target and
-- d is a side list of locations disabled by this intended move.
intent :: (Eq a, Pretty a, Show a, Ord a) =>
  G.Grid (R.Cell a) -> -- ^ grid
  MigrationOptions ->
  [RelCoord] ->        -- ^ available movements
  AbsCoord ->          -- ^ source location
  Maybe (AbsCoord, AbsCoord, [AbsCoord])
intent g opts@MigrationOptions{..} avail p =
  let values = mapMaybe f avail
      p' = snd (L.maximumBy (compare `on` fst) values)
  in
    if p' == p
      then Nothing
      else Just (p, p', links)
  where
    -- allocated cells around p
    around = G.allocatedAround g p maxDist

    -- ingoing links
    ilinks = map fst $ filter (elem p . R.allCoords . snd) around

    -- outgoing links
    olinks = R.allCoords (G.access g p)

    -- all links
    links = ilinks ++ olinks

    -- free neighborhood
    fneigh = p : G.availableAround g p (spreadDist + migDist) ++ links

    f dp = let p' = Coord.add p dp in
        if p' == p || G.inScope g p' && isNothing (G.lookup g p') then
          if any ((> maxDist) . distance p') links then
             if p == p'
                then error ("detected overlong link from or to " ++ show p)
                else Nothing
          else
            let sv = - stability * fromIntegral (distance p p')
                iv = incentive p' opts ilinks olinks fneigh
            in Just (iv + sv, p')
        else Nothing

applyMove :: Int -> (AbsCoord, AbsCoord, [AbsCoord]) -> G.Grid (R.Cell a) -> G.Grid (R.Cell a)
applyMove md (p, p', l) g =
  case G.lookup g p' of
    Nothing | not (R.dirty (G.access g p)) ->
      let g1 = G.move g p p' R.soil in
      let g2 = G.updateAround g1 p md (R.updatePointers p p') in
      foldr (\x g' -> G.update g' x R.soil) g2 l
    _ -> g

-- | Migrate cells on a grid, i.e. optimize their position, given constraints.
migrate :: (Pretty a, Show a, Ord a, RandomGen g) => [AbsCoord] -> IA a g ()
migrate nodes = do
  opts@MigrationOptions{..} <- asks migrationOpts
  -- the shuffling avoids a slight direction bias
  avail <- withRandomGen (`shuffle` neighborhood migDist)
  modify $ \st ->
    let g = _rcGrid st in
    let intents = mapMaybe (intent g opts avail) nodes in
    st { _rcGrid = foldr (applyMove maxDist) g intents }
