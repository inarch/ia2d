module Automaton.Rules where

import Language.Pin.Term
import Language.Pin.Pin
import PotRedex

-- forwarder

forwardDeltaRule :: Rule (Label a) Int
forwardDeltaRule =
  Rule (T Forwarder [V 0]) (T Delta [V 1, V 2]) (Net [Comp (V 0) (T Delta [V 1, V 2])])

forwardEpsilonRule :: Rule (Label a) Int
forwardEpsilonRule =
  Rule (T Forwarder [V 0]) (T Epsilon []) (Net [Comp (V 0) (T Epsilon [])])

forwardForwarderRule :: Rule (Label a) Int
forwardForwarderRule =
  Rule (T Forwarder [V 0]) (T Forwarder [V 1]) (Net [Comp (V 0) (V 1)])

forwardXRule :: Label a -> Int -> Rule (Label a) Int
forwardXRule l a =
  Rule (T Forwarder [V 0]) (T l xs) (Net [Comp (V 0) (T l xs)])
  where xs = map V (take a [1..])

-- epsilon

epsilonXRule :: Label a -> Int -> Rule (Label a) Int
epsilonXRule l a =
  Rule (T Epsilon []) (T l xs) (Net [Comp (T Epsilon []) x | x <- xs])
  where xs = map V (take a [1..])

epsilonEpsilonRule :: Rule (Label a) Int
epsilonEpsilonRule =
  Rule (T Epsilon []) (T Epsilon []) mempty

-- delta

deltaDeltaRule :: Rule (Label a) Int
deltaDeltaRule =
  Rule (T Delta [V 0, V 1]) (T Delta [V 2, V 3]) (Net [Comp (V 0) (V 2), Comp (V 1) (V 3)])

deltaXRule :: Label a -> Int -> Rule (Label a) Int
deltaXRule l a =
  let xs = map V $ take a [3*i+3 | i <- [0..]]
      ys = map V $ take a [3*i+4 | i <- [0..]]
      zs = map V $ take a [3*i+5 | i <- [0..]]
      c1 = V 0
      c2 = V 1
      cs1 = [Comp y (T Delta [y', y'']) | (y, y', y'') <- zip3 xs ys zs]
      cs2 = [Comp c1 (T l ys), Comp c2 (T l zs)]
  in Rule (T Delta [c1, c2]) (T l xs) (Net (cs1 ++ cs2))
