module Automaton.Migration.Options where

data MigrationOptions
  = MigrationOptions {
    spreadDist :: Int
  , maxDist    :: Int
  , migDist    :: Int
  , stability  :: Float
  , diffusion  :: Float
  , tension    :: Float
  }
  deriving Show

defaultMigrationOptions :: MigrationOptions
defaultMigrationOptions =
  MigrationOptions {
    spreadDist = 4
  , maxDist    = 8
  , migDist    = 2
  , stability  = 0.1
  , diffusion  = 5
  , tension    = 10
  }
