module Automaton.Output (
    outputOutputRule
  , outputXRule
  ) where

import           Control.Monad.State
import qualified Data.Map as M
import qualified Grid as G
import qualified PotRedex as R
import           Automaton.Types (Redex)
import           Language.Pin.Term
import           Automaton.IO.State (nextKey)
import           Automaton.IO.Types

outputOutputRule :: Redex a -> IORuleM a ()
outputOutputRule (p, (R.Node (R.Output tx) _, R.Node (R.Output ty) _)) =
  modify $ \s -> s { outputState = M.insert ty (V tx) (outputState s)
                   , grid = G.free (grid s) p }
outputOutputRule _ = error "output-output rule"

outputXRule :: Redex a -> IORuleM a ()
outputXRule (p, (R.Node (R.Output t) _, R.Node (R.Custom label) rs)) = do
  let a = length rs

  outTbl <- outputState <$> get
  let keys = take a [(nextKey outTbl)..]

  modify $ \s -> s { outputState = M.insert t (T (R.Custom label) (map V keys)) (outputState s) `M.union` M.fromList [(k, V k) | k <- keys]
                   , grid = G.accumulate R.feed (G.free (grid s) p) [(p, R.output t') | (p,t') <- zip rs keys]
                   }
outputXRule _ = error "output-X rule"
