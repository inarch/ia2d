{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module Automaton (run) where

import           Args                  (filePath)
import           Automaton.Error
import           Automaton.Input
import           Automaton.IO          (forwardIORule)
import           Automaton.IO.State
import           Automaton.IO.Types    (IORuleM, runIORule)
import qualified Automaton.IO.Types    as IOT
import           Automaton.Migration
import           Automaton.Output      (outputOutputRule, outputXRule)
import           Automaton.Rewrite
import           Automaton.Rules
import           Automaton.Types
import           Coord
import           Display.Pretty
import           Display.Stats         (printStats)
import           Display.SVG           (writeSVG)
import           Display.Terminal      (printGrid)
import qualified Grid                  as G
import qualified Language.Pin.Pin      as P
import           Language.Pin.Program
import           Language.Pin.Term     (Term (..), rename', renameWith)
import qualified PotRedex              as R

import           Control.Arrow         (second)
import           Control.Monad.Except
import           Control.Monad.Helpers
import           Data.Either           (partitionEithers)
import           Data.List             as L
import           Data.List.Shuffle
import qualified Data.Map              as M
import           Data.Maybe            (fromMaybe, mapMaybe)
import           Data.MultiSet         (MultiSet)
import qualified Data.MultiSet         as MS
import           Data.Text             (Text)
import qualified Data.Text             as T
import           Data.Time.Clock
import           Data.Tuple            (swap)
import           System.Random         (RandomGen, newStdGen)

msgOutput :: Bool
msgOutput = False

msg :: String -> IA a g ()
msg m = when msgOutput (liftIO $ putStrLn m)

-- * Auxiliary functions

time :: IO DiffTime
time = utctDayTime <$> getCurrentTime

-- * Automaton

findRule :: (Show a, Ord a) => RuleSyms a -> IA a g (P.NormalRule (R.Label a) Int)
findRule k@(x,y) =
  let err = Abort $ "The active pair " ++ show x ++ " >< " ++ show y ++ " is undefined" in
  asks ruleset >>= maybe (throwError err) return . M.lookup k

avail :: AbsCoord -> IA a g [AbsCoord]
avail i = do
  g <- _rcGrid <$> get
  maxDist <- asks miMaxDist
  return $ G.availableByDist g i maxDist -- todo: slight direction bias here

shouldCount :: (R.Node a, R.Node a) -> Bool
shouldCount (n, _) =
  case n of
    R.Node R.Forwarder _ -> False
    _                    -> True

-- TODO: unify apply, applyIORule

apply :: (Ord a, Show a) => P.NormalRule (R.Label a) Int -> Redex a -> IA a g Bool
apply rule (i, p@(ca, cb)) = do
  sp <- avail i
  g <- _rcGrid <$> get
  maxDist <- asks miMaxDist
  let ga = G.free g i -- todo: free later
  msg $ "fire " ++ show (R.label ca) ++ " " ++ show (R.label cb) ++
    " at " ++ show i ++ " space " ++ show (length sp)
  case genRule maxDist rule ga sp (i, (ca, cb)) of
    Left e        -> throwError e
    Right (g', _) -> modify $ \s -> s { _rcGrid = g' }
  return $ shouldCount p

-- TODO: handle error (maybe fatal)
applyIORule :: Show a => (Redex a -> IORuleM a ()) -> Redex a -> IA a g Bool
applyIORule rule redex@(i, ns) = do
  s <- get
  sp <- avail i
  md <- asks miMaxDist
  let ios = IOT.IORuleState (inputState s) (outputState s) (_rcGrid s) sp True
  (is, os, g, count) <- case runIORule (IOT.IORuleInput md (bound s)) ios (rule redex) of
    Left e          -> throwError e
    Right (_, ios') -> return (IOT.inputState ios', IOT.outputState ios', IOT.grid ios', IOT.count ios')
  modify $ \st -> st { inputState = is, outputState = os, _rcGrid = g }
  return $ shouldCount ns && count

selectRule :: (Ord a, Show a, Pretty a) => (R.Node a, R.Node a) -> Redex a -> IA a g Bool
selectRule (R.Node x _, R.Node y ys) =
  case (x, y) of
    -- NOTE: the order of the cases depends on the Ord instance of Label
    -- as defined in module PotRedex.
    (R.Forwarder, R.Forwarder) -> apply $ P.canonify forwardForwarderRule
    (R.Forwarder, R.Input _) -> applyIORule forwardIORule
    (R.Forwarder, R.Output _) -> applyIORule forwardIORule
    (R.Forwarder, R.Epsilon) -> apply $ P.canonify forwardEpsilonRule
    (R.Forwarder, R.Delta) -> apply $ P.canonify forwardDeltaRule
    (R.Forwarder, R.Custom _) -> apply $ P.canonify (forwardXRule y (length ys))

    (R.Input _, R.Output _) -> applyIORule inputOutputRule
    (R.Input _, R.Input _) -> applyIORule inputXRule
    (R.Input _, _) -> applyIORule inputXRule
    (R.Output _, R.Output _) -> applyIORule outputOutputRule
    (R.Output _, _) -> applyIORule outputXRule

    (R.Epsilon, R.Epsilon) -> apply $ P.canonify epsilonEpsilonRule
    (R.Epsilon, R.Delta) -> apply $ P.canonify (epsilonXRule R.Delta 2)
    (R.Epsilon, R.Custom _) -> apply $ P.canonify (epsilonXRule y (length ys))
    (R.Delta, R.Delta) -> apply $ P.canonify deltaDeltaRule
    (R.Delta, R.Custom _) -> apply $ P.canonify (deltaXRule y (length ys))

    (R.Custom sx, R.Custom sy) -> \r -> do
      -- md <- asks miMaxDist
      -- when (length sp < md * md `div` 2) $ throwError Delay -- about 25%
      f <- findRule (ruleSyms (R.Custom sx) (R.Custom sy))
      apply f r

    _ -> error $ "rewrite: unmatched case (" ++ show (x, y) ++ ")"

-- | Try to rewrite a given redex.
rewrite :: (Pretty a, Show a, Eq a, Ord a, RandomGen g) => Redex a -> IA a g ()
rewrite (i, (ca, cb)) = do
  let [cx@(R.Node _ _), cy@(R.Node _ _)] = sort [ca, cb]
  count <- selectRule (cx, cy) (i, (cx, cy))
  when count incrSeqSteps

-- | Try rewriting the net by exhaustively trying to rewrite all given redexes.
fireRedex :: (Pretty a, Ord a, Show a, RandomGen g) =>
  Redex a -> IA a g (Either (Redex a, Error a) ())
fireRedex x = catchError (Right <$> rewrite x) (\e -> return (Left (x, e)))

-- | Perform a parallel step (i.e. rewrite all independent redexes).
-- Returns the number of successful and delayed reductions.
reduce :: (Pretty a, Ord a, Show a, RandomGen g) =>
  [Redex a] -> IA a g (Int, Int, Int)
reduce xs = do
  msg $ "reduction of " ++ show (length xs) ++ " redexes"
  step0 <- seqSteps <$> get
  ys <- forM xs fireRedex
  step1 <- seqSteps <$> get

  let (errors, rewrites) = partitionEithers ys

  let fired = length rewrites ; delayed = length errors
  msg $ "rewritten " ++ show fired ++ " delayed " ++ show delayed

  case mapMaybe (fatal . snd) errors of
    e : _ -> throwError e
    _ -> return ()

  return (fired, step1 - step0, delayed)

writeGrid :: (Show a, Pretty a) => String -> IA a g ()
writeGrid suffix = do
  stepName <- withParSteps $ \steps -> return $ show steps ++ suffix
  g <- _rcGrid <$> get
  doWriteToStdout <- asks writeToStdout
  when doWriteToStdout $ liftIO $ do
    putStrLn $ "\n>>> STEP " ++ stepName
    printGrid g
  mSvgPath <- asks svgPath
  whenJust mSvgPath $ \path -> liftIO $ writeSVG (filePath path stepName) g

selectRedexes :: RandomGen g => (R.Cell a -> Bool) -> IA a g [(AbsCoord, R.Cell a)]
selectRedexes p = do
  cells <- filter (p . snd) <$> withGrid G.allocated
  withRandomGen (`shuffle` cells)

-- | IO + Forwarder Pass
specialPass :: (Ord a, Show a, Pretty a, RandomGen g) => IA a g ([(AbsCoord, R.Cell a)], Int, Int, Int)
specialPass = do
  shouldFeedInput <- feedInput <$> get
  shouldKeepOutput <- asks keepOutput

  let p | shouldFeedInput  = \r -> R.isInput (R.node1 r) || R.isInput (R.node2 r) || (R.isRedex r && R.isForward r)
        | shouldKeepOutput = \r -> R.isSpecialRedex r && not (R.isOutput (R.node1 r) || R.isOutput (R.node2 r))
        | otherwise        = R.isSpecialRedex

  ioRedexes <- selectRedexes p
  (f, s, d) <- reduce (map (second R.splitRedex) ioRedexes)
  -- writeGrid "io"

  -- stop feeding input when there are no more `in` cells (or forwarders)
  when (shouldFeedInput && null ioRedexes) (modify (\s -> s { feedInput = False }))

  return (ioRedexes, f, s, d)

-- | Forwarder Contraction Pass
contractionPass :: (Ord a, Show a, Pretty a, RandomGen g) => IA a g ()
contractionPass = do
  fwdCells <- selectRedexes R.isForward
  md <- asks miMaxDist
  let u (p, r) g =
       let around = G.allocatedAround g p md
           inodes = map fst $ filter (elem p . R.allCoords . snd) around
       in case (R.allCoords r, inodes) of
            ([o], [i]) ->
              if not (R.dirty (G.access g p)) && distance i o <= md then
                G.update (G.update (G.free g p) i (R.soil . R.updatePointers p o)) o R.soil
               else g
            _ -> error "forwarder contraction"
   in modify $ \s -> s { _rcGrid = foldr u (_rcGrid s) fwdCells }

-- | Migration Pass
migrationPass :: (Ord a, Show a, Pretty a, RandomGen g) => IA a g ()
migrationPass = do
  migCells <- selectRedexes (not . R.dirty)
  migrate (map fst migCells)
  writeGrid "m"

iteration :: (RandomGen g, Ord a, Pretty a, Show a) => IA a g Bool
iteration = do
  modify $ \s -> s { _rcGrid = G.updateAll (_rcGrid s) R.clean }

  (ioRedexes, fired1, seq1, delayed1) <- specialPass

  shouldSkipContrPass <- asks skipContrPass
  unless shouldSkipContrPass contractionPass

  migrationPass

  incrParSteps

  shouldFeedInput <- feedInput <$> get
  if not shouldFeedInput then do
    cells3 <- withGrid G.allocated
    redexes <- selectRedexes R.isNormalRedex
    if null cells3 || (null redexes && null ioRedexes) then
      return True
     else do
      -- Interaction Pass
      (fired2, seq2, delayed2) <- reduce (map (second R.splitRedex) redexes)
      if fired1 + fired2 == 0 then do
        fs <- failedSteps <$> get
        if fs >= 5 then do
          (x, y) <- withGrid G.size
          throwError $
            if length cells3 >= x * y `div` 2 then -- more than 50% allocated
              Abort "Out of memory (increase the grid size)"
             else
              Abort "Memory congestion (no progress after 5 reduction passes)"
         else
          modify $ \s -> s { failedSteps = fs + 1 }
       else
        modify $ \s -> s { failedSteps = 0 }
      tell [ParStep (seq1 + seq2) (delayed1 + delayed2)]
      writeGrid ""
      return False
   else
    return False

-- * Rule preprocessing and grid setup

class IsLabel a where
  toLabel :: a -> R.Label a

instance IsLabel Text where
  toLabel x | x == "delta"   = R.Delta
            | x == "epsilon" = R.Epsilon
            | otherwise      = R.Custom x

labelTerm :: IsLabel a => Term a b -> Term (R.Label a) b
labelTerm (V x) = V x
labelTerm (T x xs) = T (toLabel x) $ map labelTerm xs

labelRule :: IsLabel a => P.NormalRule a Int -> P.NormalRule (R.Label a) Int
labelRule (P.NormalRule t1 t2 (P.Net pl)) =
  let f (P.Comp u1 u2) = P.Comp (labelTerm u1) (labelTerm u2) in
  P.NormalRule (labelTerm t1) (labelTerm t2) (P.Net (map f pl))

convertTerm :: MultiSet Int -> Term (R.Label a) Int -> IA a g (Term (R.Label a) Int)
convertTerm ms (V x) =
  case MS.occur x ms of
    1 -> do
      modify $ \s -> s { outputState = M.insert x (V x) (outputState s)
                       , outputs = x : outputs s }
      return $ T (R.Output x) []
    2 -> do
      modify $ \s -> s { bound = x : bound s }
      return (V x)
    _ -> return (V x)
convertTerm ms (T f xs) = do
  xs' <- mapM (convertTerm ms) xs
  return $ T f xs'

preprocessRules ::
  (Eq a, Ord a, Show a, Pretty a, IsLabel a, Ord b) =>
  [P.Rule a b] -> M.Map (RuleSyms a) (P.NormalRule (R.Label a) Int)
preprocessRules =
    M.fromList . map (makeRule . labelRule . P.canonify . renameVars)
  where
    renameVars r = rename' (renamingFrom 0 (P.ruleTerms r)) r
    makeRule r@(P.NormalRule (T f _) (T g _) _) = (ruleSyms f g, r)
    makeRule _ = error "incorrect rule"

populateGrid ::
  (Eq a, Show a, Pretty a, IsLabel a, Ord b, Show b, Pretty b) =>
  [P.Net a b] -> IA a g ()
populateGrid = void . foldM writeNet (0 :: Int)

writeNet ::
  (Eq a, Show a, Pretty a, IsLabel a, Ord b, Show b, Pretty b) =>
  Int -> P.Net a b -> IA a g Int
writeNet from (P.Net cs) = do
  let m = renamingFrom from (concatMap P.compTerms cs)
  modify $ \s -> s { mappings = mappings s ++ [M.fromList . map (second pretty . swap) . M.toList $ m] }

  let cs' = P.unflatten . map (rename' m) $ cs
  msg $ "input: " ++ intercalate ", " (map pretty cs')

  g <- _rcGrid <$> get
  let locations = G.availableByDist g (G.center g) 4096 -- todo:hardcoded

  let netVars = P.allVars cs'

  let f l (P.Comp t u) = do
       inTbl <- inputState <$> get
       let keys = [(nextKey inTbl)..]
       -- write terms into input table
       t' <- convertTerm netVars (labelTerm t)
       u' <- convertTerm netVars (labelTerm u)
       let entries@[(k1, _), (k2, _)] = zip keys [InputTerm t', InputTerm u']
       modify $ \s -> s { inputState = inTbl `M.union` M.fromList entries }
       -- write redex on grid
       let rx = R.cell (R.input k1) (R.input k2)
       let p : q = l
       modify $ \s -> s { _rcGrid = G.allocate (_rcGrid s) p rx }
       return q

  foldM_ f locations cs'
  return . succ . snd . M.findMax $ m

prettyOutputs :: (Eq a, Pretty a) => RunState a g -> [String]
prettyOutputs s =
    map disp (outputs s)
  where
    disp x = pretty' True $ renameWith f (P.Comp (V x) (readOutput (outputState s) x))
    f x = fromMaybe (T.pack $ "$" ++ show x) (M.lookup x m)
    m = M.map T.pack $ mconcat (mappings s)

-- * Interface

-- | Run the automaton with the given rule set and grid as input.
run :: (Ord b, Show b, Pretty b) => MInput Text -> Program Text b -> IO ()
run opt prog = do
  g <- case initGen opt of
         Nothing -> newStdGen
         Just g  -> return g
  putStrLn $ "seed: " ++ show g

  let i = opt { ruleset = preprocessRules (ruleSet prog) }

  let s = RunState {
      _rcRnd      = g
    , seqSteps    = 0
    , parSteps    = 0
    , failedSteps = 0
    , _rcGrid     = G.create ((1, 1), gridSize opt)
    , outputState = mempty
    , inputState  = mempty
    , mappings    = mempty
    , outputs     = mempty
    , bound       = mempty
    , feedInput   = feedFlag opt
    }

  (err, s', o) <- executeIA i s $ do
    populateGrid (nets prog)
    writeGrid ""
    t0 <- liftIO time
    tell [Timestamp t0]
    maxSteps <- asks miMaxSteps
    b <- case maxSteps of
      Nothing -> untilM iteration >> return True
      Just n  -> iterateM n iteration
    t1 <- liftIO time -- todo: errors should not bypass this
    tell [Timestamp t1]
    unless b (throwError $ Abort "Step limit reached")

  let _printStats = printStats i
  case err of
    Right ()       -> _printStats (Right (prettyOutputs s')) s' o
    Left (Abort e) -> _printStats (Left e)  s' o
    Left e         -> _printStats (Left (show e))  s' o
