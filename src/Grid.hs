module Grid where

import           Data.Array
import           Data.Maybe (fromMaybe, mapMaybe)

import           Coord

type Grid a = Array AbsCoord (Maybe a)

-- | Create a grid of a given size.
create :: Scope -> Grid a
create bs = array bs [(i, Nothing) | i <- range bs]

-- | Return the dimensions of the grid.
size :: Grid a -> (Int, Int)
size g =
  let ((xmin, ymin), (xmax, ymax)) = bounds g in
  (xmax - xmin + 1, ymax - ymin + 1)

-- | Return the center of the grid.
center :: Grid a -> AbsCoord
center g =
  let ((xmin, ymin), (xmax, ymax)) = bounds g in
  ((xmin + xmax) `div` 2, (ymin + ymax) `div` 2)

inScope :: Grid a -> (Int, Int) -> Bool
inScope g = Coord.scope (bounds g)

-- | Raw unchecked access to a cell of the grid.
access :: Grid a -> AbsCoord -> a
access g p = fromMaybe (error "access") (g ! p)

-- | Raw unchecked access to a location of the grid.
lookup :: Grid a -> AbsCoord -> Maybe a
lookup g p = g ! p

overlay :: Grid a -> Grid a -> Grid a
overlay a1 a2 =
  create bs // filter f (assocs a1 ++ assocs a2)
    where
      bs = ((xmin', ymin'), (xmax', ymax'))
      ((xmin1, ymin1), (xmax1, ymax1)) = bounds a1
      ((xmin2, ymin2), (xmax2, ymax2)) = bounds a2
      xmin' = min xmin1 xmin2
      xmax' = max xmax1 xmax2
      ymin' = max ymin1 ymin2
      ymax' = max ymax1 ymax2
      f (_, Nothing) = False
      f _ = True

clip :: Grid a -> AbsCoord -> Int -> Grid a
clip g (x,y) d =
  let ((xmin, ymin), (xmax, ymax)) = bounds g in
  let xmin' = max xmin (x-d)
      xmax' = min xmax (x+d)
      ymin' = max ymin (y-d)
      ymax' = min ymax (y+d)
  in ixmap ((xmin', ymin'), (xmax', ymax')) id g

-- * Helpers

popAllocated :: (b, Maybe a) -> Maybe (b, a)
popAllocated (p, Just v) = Just (p, v)
popAllocated _ = Nothing

popAvailable :: (b, Maybe a) -> Maybe b
popAvailable (p, Nothing) = Just p
popAvailable _ = Nothing

-- | Return all allocated cells.
allocated :: Grid a -> [(AbsCoord, a)]
allocated g = mapMaybe popAllocated $ assocs g

-- | Return allocated cells within a provided radius.
allocatedAround :: Grid a -> AbsCoord -> Int -> [(AbsCoord, a)]
allocatedAround g p d =
    filter (\(p', _) -> distance p p' <= d) . allocated $ clip g p d

allocatedByDist :: Grid a -> AbsCoord -> Int -> [(AbsCoord, a)]
allocatedByDist g p d =
    mapMaybe (\x -> case g ! x of Just v -> Just (x, v); Nothing -> Nothing) .
    filter (inScope g) . map (Coord.add p) $ neighborhood d

-- | Return all free cells.
available :: Grid a -> [AbsCoord]
available g = mapMaybe popAvailable $ assocs g

-- | Return free cells within a provided radius.
availableAround :: Grid a -> AbsCoord -> Int -> [AbsCoord]
availableAround g p d =
  filter (\p' -> distance p p' <= d) . available $ clip g p d

availableByDist :: Grid a -> AbsCoord -> Int -> [AbsCoord]
availableByDist g p d =
    filter (\x -> case g ! x of Nothing -> True; _ -> False) .
    filter (inScope g) . map (Coord.add p) $ neighborhood d

-- * Grid mutation

-- | Free one location in a grid.
free :: Grid a -> AbsCoord -> Grid a
free g p =
  case g ! p of
    Nothing -> error "free"
    Just _ -> g // [(p, Nothing)]

-- | Allocate one location in a grid.
allocate :: Grid a -> AbsCoord -> a -> Grid a
allocate g p c =
  case g ! p of
    Nothing -> g // [(p, Just c)]
    Just _ -> error "allocate"

-- | Update a cell at a given position.
update :: Grid a -> AbsCoord -> (a -> a) -> Grid a
update g p f =
  case g ! p of
    Nothing -> error "update"
    Just c -> g // [(p, Just (f c))]

accumulate :: (e -> a -> e) -> Grid e -> [(AbsCoord, a)] -> Grid e
accumulate f =
  let u (Just c) n = Just (f c n)
      u _ _ = error "accumulate"
  in accum u

-- | Update all allocated cells.
updateAll :: Grid a -> (a -> a) -> Grid a
updateAll g f =
  g // map (\(p', v) -> (p', Just (f v))) (allocated g)

-- | Update all allocated cells in the neighborhood of a certain location.
updateAround :: Grid a -> AbsCoord -> Int -> (a -> a) -> Grid a
updateAround g p d f =
  g // map (\(p', v) -> (p', Just (f v))) (allocatedAround g p d)

-- | Move a cell from one location to another.
move :: Grid a -> AbsCoord -> AbsCoord -> (a -> a) -> Grid a
move g p p' f =
  case g ! p' of
    Just _ -> error "move target"
    Nothing ->
      case g ! p of
        Nothing -> error "move source"
        Just c -> g // [(p, Nothing), (p', Just (f c))]
