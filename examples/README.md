# Example programs and library

The example programs are based on library code that is stored under the `lib` directory.

    bool.inet - operations on booleans
    lambda.inet - useful definitions for implementing higher-order functions
    maybe.inet - operations on maybe type
    tuple.inet - operations on tuples
    list/base.inet - various operations on lists (generation, concatenation, map, fold, flatten, length, etc.)
    list/maybe.inet - operations on lists of maybe values
    list/msort.inet - merge sort implementation
    nat/unary.inet - operations on natural numbers in unary encoding
    nat/necklace.inet - operations on natural numbers in necklace encoding (see below)
    nat/binary.inet - operations on natural numbers in binary encoding
    segment/base.inet - helper functions for segments (binary tree's of values)
    segment/list.inet - convert from segment to list
    segment/lop.inet - local operations on segments
    segment/bsort.inet - bitonic sort implementation

## Necklace encoding of natural numbers

A number x is encoded as a net loop(x1, x2) where x2 points to the unary encoding of x (succ(succ(succ(..)))) and x1 is attached instead of the zero() constructor, such that when adding two numbers, we can simply "open" the loops and attach the beginning of one number to the end of the other, producing a new loop.

## Example programs

The repository of example programs consists mostly of small programs that use the libraries described above and apply it to examplep input. These programs can be run using `ia2d`.

    bool_example.inet - fold list of boolean using xor
    bsort_example.inet - apply bitonic sort
    bsort_function.inet - generate bitonic sorter for segment of given size
    dilute_example.inet - apply dilusion to a segment
    list_example.inet - apply various list operations
    maybe_example.inet - apply maybe operations
    msort_example.inet - apply merge sort
    nat_binary_example.inet - apply operations on binary numbers
    nat_necklace_example.inet - apply operations on necklace numbers
    nat_unary_example.inet - apply operations on unary numbers
