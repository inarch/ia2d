#!/bin/sh
PROGRAM=examples/bsort_example.inet
for INPUT_SIZE in 1 2 3 4 5 6 7; do
  for i in `seq 1`; do
    echo "size: $INPUT_SIZE, run #$i"
    stack exec -- bsort_driver \
      $PROGRAM \
      --link-range=10 \
      --hop-range=2 \
      --spread-distance=6 \
      --grid-size=64x64 \
      --quiet \
      $INPUT_SIZE # | egrep '(SEQ|PAR)'
  done
done
