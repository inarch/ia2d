#!/bin/sh
GRID_SIZE=32x32
for each in `ls -1 examples/*.inet`; do
  echo ">> Input program: $each"
  dist/build/ia2d/ia2d --grid-size=$GRID_SIZE $each
  echo
done
