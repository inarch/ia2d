#!/bin/sh
for target in `grep executable ia2d.cabal | cut -d ' ' -f 2`; do
  echo ":ctags" | cabal repl $target
  mv tags tags.$target
done
cat tags.* | sort | uniq > tags
rm tags.*
