#!/bin/sh
for n in 8 16 32 64 128 256 512 1024; do
  echo "n: $n"
  TEMP=temp_$n.inet
  rm $TEMP
  echo "import \"examples/lib/nat/unary.inet\"" >> $TEMP
  echo "import \"examples/lib/list/even_odd_sort.inet\"" >> $TEMP
  stack exec -- even_odd_sort_driver $n >> $TEMP
  echo "~even_odd_sort(r);" >> $TEMP
  echo
done
