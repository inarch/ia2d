.PHONY: doc clean hlint tags

clean:
	find src -name \*~ -exec rm {} \;
	rm -rf dist

tags:
	scripts/mktags.sh

doc:
	cabal haddock --executables

hlint:
	find src -name \*\.hs -exec hlint {} \;

README.html: README.md
	pandoc $< -s -o $@
