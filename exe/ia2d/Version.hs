module Version (
    version
  , showVersion
  ) where

import Data.Version (showVersion)

import Paths_ia2d (version)
