{-# LANGUAGE LambdaCase      #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import           Args                     hiding (options)
import qualified Args
import           Display.Pretty
import           Language.Pin
import           Language.Pin.Error
import           Language.Pin.Net
import           Language.Pin.Parser.Pin  hiding (program)
import           Language.Pin.Parser.Term
import           Language.Pin.Pin         (Rule (..))
import           Language.Pin.Valid
import           Repl
import           Version

import           Control.Monad            (forM_)
import           Control.Monad.State
import           Data.List                (isPrefixOf)
import           Data.Maybe               (fromMaybe)
import           Data.Monoid              ((<>))
import           Data.Text                (Text)
import qualified Data.Text                as T
import           System.Console.GetOpt
import           System.Environment       (getProgName)
import           System.Exit
import           Text.Parsec

main :: IO ()
main = do
  (opts, args, errors) <- getOptions

  unless (null errors) $ do
    invocationHelp
    exitFailure

  case (enterRepl opts, args) of
    (True, _) -> interactive
    (False, []) -> invocationHelp >> exitFailure
    (False, _:_) ->
      forM_ args $ \p -> do
        eprg <- loadProg p
        case eprg of
          Right prg -> mainWithInput prg (nets prg)
          Left e    -> die . showError $ e

invocationHelp :: IO ()
invocationHelp = do
  progName <- getProgName
  putStrLn $ unlines [
      intro
    , ""
    , "Usage:"
    , ""
    , "  " ++ progName ++ " [options] [<file1> ... <fileN>]"
    , ""
    , "Options:"
    , usageInfo "" Args.options
    ]

intro :: String
intro = "ia2d " ++ showVersion version

-- Interactive mode

data ReplState
  = ReplState {
    options :: Options
  , program :: Program Text Text
  }

defaultReplState :: ReplState
defaultReplState = ReplState defaultOptions mempty

interactive :: IO ()
interactive =
  do
    sayHello
    void $ repl "> " defaultReplState $ \case
      ':' : cmd -> command cmd
      line      -> input line
  where
    command :: String -> StateT ReplState IO Bool
    command input =
      case match commands input of
        Left e -> do
          liftIO $ print e -- TODO: pretty-print
          return True
        Right ReplCmd{..} ->
          replCmdFunc (drop 1 . words $ input)

    input :: String -> StateT ReplState IO Bool
    input line = do
      parseAndReduce (T.pack line)
      return True

    sayHello = do
      putStrLn $ "ia2d " ++ showVersion version ++ " (interactive mode)"
      putStrLn $ "Type `:help<ENTER>` at the prompt to get help."
      putStrLn $ "Load a program using the `:load` command."
      putStrLn $ "Type an initial net at the prompt and press <ENTER> to reduce the net using the loaded program."

parseAndReduce :: Text -> StateT ReplState IO ()
parseAndReduce input = do
  p <- program <$> get
  case parseNet input of
    Left e -> liftIO $ print e
    Right n -> do
      opts <- options <$> get
      liftIO $ runWithInput opts p [n]

parseNet :: Text -> Either ParseError (Net Text Text)
parseNet = fmap Net . parse (spaces *> net var <* eof) "Repl"

parseAndDefine :: Text -> StateT ReplState IO ()
parseAndDefine raw =
  case parse (spaces *> rule var <* eof) "Repl" raw of
    Left e -> liftIO $ print e
    Right r ->
      liftIO (evalLoad (checkRule r)) >>=
        \case
          Left e' -> liftIO (print e')
          Right _ ->
            let p = Program [r] []
            in
            modify $ \s -> s { program = program s <> p }

data ReplCmd
  = ReplCmd {
    replCmdName :: String
  , replCmdFunc :: [String] -> StateT ReplState IO Bool
  , replCmdDesc :: String
  }

data ReplErr
  = ReplErr String
  | ReplErrNoMatch String
  | ReplErrAmbMatch String
  deriving (Eq, Show)

commands :: [ReplCmd]
commands =
    [ ReplCmd "quit"  cmdQuit  "quit ia2d"
    , ReplCmd "load"  cmdLoad  "load rules from program, e.g. `:load myprog.txt`"
    , ReplCmd "def"   cmdDef   "define rule, e.g. `:def f(x) >< g(y) => x~y`"
    , ReplCmd "clear" cmdClear "clear rules"
    , ReplCmd "rules" cmdRules "list rules"
    , ReplCmd "{"     cmdML    "enter multi-line expression, end with `}`"
    , ReplCmd "flags" cmdFlags "show/set flags, use `:flags` to display flags, use `:flags --feed` to set `--feed` flag"
    ]
  where
    cmdQuit = \_ -> return False

    cmdLoad =
      \case
        [path] -> do
          result <- liftIO $ loadProg path
          curProg <- program <$> get
          case result of
            Left e -> liftIO $ putStr (showError e)
            Right prg -> do
              modify $ \s -> s { program = curProg <> prg }
              liftIO $ putStrLn "Program loaded, type :rules at the prompt to inspect it."
          return True
        _ -> do
          liftIO $ putStrLn "one argument expected"
          return True

    cmdClear = \_ -> do
      modify $ \s -> s { program = mempty }
      return True

    cmdRules = \_ -> do
      (Program rs _) <- program <$> get
      liftIO $ putStr . unlines . map pretty $ rs
      return True

    cmdDef = \xs -> do
      let def = unwords xs
      parseAndDefine (T.pack def)
      return True

    cmdML = \_ -> do
      xs <- multiline "| " ":}"
      parseAndReduce (T.pack . unlines $ xs)
      return True

    cmdFlags =
      \case
        [] -> do
          opts <- options <$> get
          (liftIO . putStrLn . showMInput) opts
          return True
        flags -> do
          s <- get
          let opts = options s
          let (opts', _, _) = applyFlags flags (options s)
          mapM_ (liftIO . putStrLn . showMInput) [opts']
          modify $ \s -> s { options = opts' }
          return True

match :: [ReplCmd] -> String -> Either ReplErr ReplCmd
match commands input =
  case words input of
    [] -> Left $ ReplErrNoMatch input
    "help" : _ -> Right $ ReplCmd "help" cmdHelp ""
      where
        cmdHelp = \_ -> do
          liftIO $
            forM_ commands $ \ReplCmd{..} -> do
              putStrLn $ ":" <> replCmdName
              putStrLn mempty
              putStr "  "
              putStrLn replCmdDesc
              putStrLn mempty
          return True
    prefix : args ->
      case filter (\ReplCmd{..} -> prefix `isPrefixOf` replCmdName) commands of
        []  -> Left $ ReplErrNoMatch input
        [c] -> Right c
        _   -> Left $ ReplErrAmbMatch input
