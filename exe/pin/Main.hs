module Main where

import Control.Monad (forM_)
import System.Environment (getArgs)
import Data.List (intercalate)
import Data.Set (Set)
import qualified Data.Set as S
import Data.Bifoldable

import Language.Pin
import Language.Pin.Term
import Language.Pin.Pin
import Display.Pretty

-- LaTeX output

latexTerm :: Show a => Term a Int -> String
latexTerm (V x) = "x_{" ++ show x ++ "}"
latexTerm (T f xs) = "\\mathsf{" ++ show f ++ "}(" ++ intercalate ", " (map latexTerm xs) ++ ")"

latexRule :: Show a => Rule a Int -> String
latexRule (Rule t u (Net cs)) = latexTerm t ++ " \\Join " ++ latexTerm u ++ " \\Rightarrow " ++ intercalate ", " (map latexComp cs)

latexComp :: Show a => Comp a Int -> String
latexComp (Comp t u) = latexTerm t ++ "\\sim{}" ++ latexTerm u

f :: FilePath -> IO ()
f path = do
  ep <- loadProg path
  case ep of
    Right p -> showProg p
    Left e -> print e

showProg, showRules, showNets, showFuns :: (Eq a, Ord a, Ord b, Show a, Show b, Pretty a, Pretty b) => Program a b -> IO ()

showProg p = do
  showRules p
  showNets p
  showFuns p

showRules (Program rs _) =
  forM_ (zip [0..] rs) $ \(i, r) -> do
    putStrLn $ "RULE #" ++ show i
    putStrLn $ "       rule: " ++ pretty r
    putStrLn $ "  canonical: " ++ pretty (canonify r)
    -- putStrLn $ "  " ++ latexRule r'

showNets (Program _ ns) = do
  mapM_ (putStrLn . (++) "net: " . intercalate ", " . map pretty . components) ns
  mapM_ (putStrLn . (++) "net: " . intercalate ", " . map (latexComp . rename) . components) ns

fun :: Ord a => Rule a b -> Set a
fun = bifoldMap S.singleton (const mempty)

showFuns (Program rs _) = putStrLn $ intercalate "\n" . map ((++) "fun: " . pretty) . S.toList . S.unions . map fun $ rs

main :: IO ()
main = do
  args <- getArgs
  mapM_ f args
