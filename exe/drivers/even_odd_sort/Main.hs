{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Display.Pretty
import           Language.Pin.Term

import           Control.Monad      (replicateM)
import           Data.Monoid        ((<>))
import           Data.Text          (Text)
import qualified Data.Text          as T
import           System.Environment (getArgs)
import           System.Random

fromList :: [Term Text Text] -> Term Text Text
fromList (x:xs) = T "Cons" [x, fromList xs]
fromList []     = T "Nil" []

nat :: Int -> Term Text a
nat 0 = T "zero" []
nat n = T "succ" [nat (n - 1)]

main :: IO ()
main = do
  args <- getArgs
  let n = case args of [] -> 64; (x:_) -> read x
  xs <- replicateM n (randomRIO (0, 3) :: IO Int)
  putStrLn . pretty' False . fromList . map nat $ xs
