{-# LANGUAGE OverloadedStrings #-}

module Main where

import           IA2d

import           Control.Monad      (void)
import           Data.Text          (Text)
import           System.Environment (getProgName)
import           System.Random
import           Text.Read          (readMaybe)

unary :: Int -> Term Text a
unary 0 = T "zero" []
unary n = T "succ" [unary (n - 1)]

leaf :: Int -> Term Text a
leaf n = T "leaf" [unary n]

tree :: [Int] -> Term Text a
tree [x,y] = T "node" [leaf x, leaf y]
tree xs =
  let n = length xs
      (left, right) = splitAt (n `quot` 2) xs
  in  T "node" [tree left, tree right]

run :: Options -> FilePath -> Int -> IO ()
run options path e = do
  let inputSize = 2^e
  print inputSize

  -- generate list of random number
  --g <- newStdGen
  let g = read "0 0" :: StdGen
  let list = take inputSize (randomRs (0, 3) g)

  -- input net
  let input = Net [Comp (T "bsort" [V "r"]) (tree list)] :: Net Text Text

  -- pretty print input program
  print (pretty input)

  -- run automaton
  prog <- (\(Right p) -> p) <$> loadProg path
  void $ runWithInput options prog [input]

main :: IO ()
main = do
  (options, args, errs) <- getOptions
  if null errs then
    case args of
      [path, sizeStr]
        | Just e <- readMaybe sizeStr :: Maybe Int
        , e > 0
        -> run options path e
      _ -> do
        progName <- getProgName
        putStrLn "Usage:"
        putStrLn $ "  " ++ progName ++ " <path> <e>"
        putStrLn "where e must be a positive integer greater than 0 and 2^e is the actual input size"
   else do
    mapM_ (\e -> putStrLn $ "Error: " ++ e) errs
