{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Text (Text)
import System.Environment

import Grid (Grid)
import Grid.Builder
import PotRedex
import Display.SVG (writeSVG)

g :: Grid (Cell Text)
g =
  setupGrid size $ do
    node (Custom "f") xs >< node (Custom "g") [(1,1), (1,4)] @@ (1,2)
    node (Custom "alpha") [(2,2),(4,2)] >< node (Custom "beta") [(1,2),(6,2)] @@ (3,2)
    node (Custom "f") [(2,1),(2,4)] >< node (Custom "h") [(4,1),(4,4)] @@ (3,3)
    node (Custom "loop") [(5,4)] >< node (Custom "loop") [(5,4)] @@ (5,4)
    pre (Custom "f") [(5,3)] @@ (4,4)
    pre (Custom "f") [(5,3)] @@ (5,3)
  where
    size = (6,4)
    xs = [(1,1), (1,3)]

main :: IO ()
main = do
  [path] <- getArgs
  writeSVG path g
