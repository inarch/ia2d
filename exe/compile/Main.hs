{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}

module Main where

import           Prelude              hiding (curry)

import           Display.Pretty
import           Language.Pin.Net
import           Language.Pin.Pin
import           Language.Pin.Term    hiding (rename, rename')
import           Parse

import           Control.Arrow        (second)
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Char            (isLower)
import           Data.List            (nub, partition)
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Monoid          ((<>))
import           Data.Text            (Text)
import qualified Data.Text            as T
import           System.Environment   (getArgs, getProgName)
import           System.Exit

-- * Overlap

overlaps :: Eq a => Term a b -> Term a b -> Bool
V _    `overlaps` V _                = True
V _    `overlaps` T _ _              = True
T _ _  `overlaps` V _                = False
T f xs `overlaps` T g ys | f /= g    = False
                         | otherwise = all (uncurry overlaps) (zip xs ys)

-- * Translation

-- |Preprocess term, i.e. classify function symbols into function
-- applications (lower-case first letter) and constructors (upper-case
-- first letter)
preprocess :: Term Text Text -> Expr
preprocess (V x) = Var x
preprocess (T t ts) =
    (if isApp t then App else Con) t (map preprocess ts)
  where
    isApp = T.all isLower . T.take 1

-- |Translate left-hand side of a definition
tlhs :: Expr -> (Term Text Text, Term Text Text)
tlhs (App f (Con c xs : ys)) =
    (T c (conv xs), T f (conv ys ++ [V "$r"]))
  where
    conv :: [Expr] -> [Term Text Text]
    conv = map (\(Var x) -> V x)
tlhs _ = error "tlhs: LHS must be of form f(C(x_1, ..., x_i), x_j, ..., x_k)"

consume :: (Monad m, MonadState [a] m) => m a
consume = do
  (x:xs) <- get
  put xs
  return x

newtype Tr a r
  = Tr {
    runTr :: ReaderT ([(a, Int)], [a]) (State [a]) r
  } deriving (
    Functor
  , Applicative
  , Monad
  , MonadReader ([(a, Int)], [a])
  , MonadState [a]
  )

evalTr :: ([(a, Int)], [a]) -> [a] -> Tr a r -> r
evalTr bound fresh (Tr f) = flip evalState fresh . flip runReaderT bound $ f

-- |Translate right-hand side of definition
trhs :: Expr -> Tr Text (Term Text Text, [Comp Text Text])
trhs (Var x) = do
  (bound, local) <- ask
  case lookup x (filter ((`notElem` local) . fst) bound) of
    Just arity -> curry x arity
    Nothing    -> return (V x, mempty)
trhs (Con c ts) = do
  tts <- mapM trhs ts
  let ts' = map       fst  tts
  let ns  = concatMap snd  tts
  return (T c ts', ns)
trhs (App f ts) = do
  tts <- mapM trhs ts
  let (t':ts') = map       fst  tts
  let ns       = concatMap snd  tts

  fresh <- consume

  (_, bound) <- ask
  if f `elem` bound
    then
      -- call λ-abstraction
      let app :: Text -> [Term Text Text] -> Tr Text (Term Text Text, [Comp Text Text])
          app r []     = return (V r, [])
          app g (a:as) = do
            x <- consume
            (r, os) <- app x as
            return (r, Comp (V g) (T "app" [a, V x]) : os)
      in
      app f (t':ts') >>= \(t, ps) -> return (t, ps ++ ns)
    else
      -- function call
      return (V fresh, Comp t' (T f (ts' ++ [V fresh])) : ns)
trhs (Abs x e) = do
  r <- consume
  s <- consume
  (e, ns) <- trhs e
  return
    (V r, [ Comp (V r) (T "fun" [V x, V s])
          , Comp (V s) e ]
          ++ ns)

curry :: Text -> Int -> Tr Text (Term Text Text, [Comp Text Text])
curry f n = do
    f' <- consume
    as <- map V <$> replicateM n consume
    return (V f', [Comp (V f') (T "fun" [T f as, rec as])])
  where
    rec :: [Term Text Text] -> Term Text Text
    rec [r] = r
    rec (a:as) = T "fun" [a, rec as]

-- |Construct delta-tree for duplicating values
deltaTree :: a -> Int -> State [a] ([Comp Text a], [a])
deltaTree x 1 = return ([], [x])
deltaTree x 2 = do
  x1 <- consume
  x2 <- consume
  return ([Comp (V x) (T "delta" [V x1, V x2])], [x1, x2])
deltaTree x n = do
  let (n', r) = n `quotRem` 2
  xi <- consume
  xj <- consume
  (n1, xs1) <- deltaTree xi n'
  (n2, xs2) <- deltaTree xj (n' + r)
  return ( Comp (V x) (T "delta" [V xi, V xj]) : n1 ++ n2
         , xs1 ++ xs2
         )

epsilon :: a -> Comp Text a
epsilon x = Comp (V x) (T "epsilon" mempty)

rename :: Text -> State (Map Text [Text]) Text
rename x = get >>= \m ->
  case M.lookup x m of
    Nothing      -> return x
    Just (x':xs) -> put (M.adjust (const xs) x m) >> return x'
    Just []      -> return x

subst :: Term Text Text -> State (Map Text [Text]) (Term Text Text)
subst (V x)    = V <$> rename x
subst (T f ts) = T f <$> mapM subst ts

translate :: [(Text, Int)] -> (Term Text Text, Expr) -> Rule Text Text
translate glob (lhs@(T f _), rhs) =
    Rule t1 t2 (Net cs)
  where
    cs = Comp (V "$r") t3' : ns' ++ epsilons ++ deltaTrees

    (t1, t2) = (tlhs . preprocess) lhs
    (t3, ns) = evalTr (glob, M.keys vL) (fresh "x") (trhs rhs)
    (t3', ns') =
      evalState f m
      where
        m = M.fromList . map snd $ dup
        f = do x <- subst t3
               y <- mapM (\(Comp l r) -> Comp <$> subst l <*> subst r) ns
               return (x, y)

    vL = vars' lhs
    vR = vars_ rhs

    -- unused variables
    epsilons = map (epsilon . fst) (M.toList $ vL M.\\ vR)

    -- duplicated variables
    dup = evalState (mapM deltaTree' (M.toList vR)) (fresh "d")
    deltaTree' (x, n) = deltaTree x n >>= \(ns, xs) -> return (ns, (x, xs))
    deltaTrees = concat . map fst $ dup

    fresh prefix = map (\i -> "$" <> prefix <> T.pack (show i)) [1..]

    vars_ :: Expr -> Map Text Int
    vars_ (Abs _ _) = mempty
    vars_ (Var x) = M.singleton x 1
    vars_ t =
      case t of
        App f es | f `elem` M.keys vL -> M.unionWith (+) (go es) (M.singleton f 1)
                 | otherwise          -> go es
        Con _ es -> go es
      where
        go = M.unionsWith (+) . fmap vars_

data Options
  = Options {
    optIncludeFunAppRule :: Bool
  } deriving (
    Eq
  , Show
  )

compile :: Options -> FilePath -> IO ()
compile options path = parseFile path >>= either putStrLn f
  where
    f defs = mapM_ (putStrLn . (++ ";") . pretty) $ rules
      where
        glob = nub . map (\(T f ts, _) -> (f, length ts)) $ defs
        rules =
          map (translate glob) defs
          ++ if optIncludeFunAppRule options then [funAppRule] else []

-- * Rules

deltaDeltaRule :: Rule Text Text
deltaDeltaRule =
  Rule
    (T "delta" [V "$0", V "$1"])
    (T "delta" [V "$2", V "$3"])
    (Net [ Comp (V "$0") (V "$2")
         , Comp (V "$1") (V "$3")
         ])

deltaXRule :: Text -> [Text] -> Rule Text Text
deltaXRule f ys =
  Rule
    (T f (map V ys))
    (T "delta" (map V xs))
    (Net (map g xs ++ map h ys))
  where
    xs = ["$1", "$2"]

    g :: Text -> Comp Text Text
    g x = Comp (V x) (T f (map (\y -> V (x <> "_" <> y)) ys))

    h :: Text -> Comp Text Text
    h y = Comp (V y) (T "delta" [V (x <> "_" <> y) | x <- xs])

epsilonXRule :: Text -> [Text] -> Rule Text Text
epsilonXRule f xs =
  Rule
    (T f (map V xs))
    (T "epsilon" [])
    (Net (map g xs))
  where
    g x = Comp (V x) (T "epsilon" [])

funAppRule :: Rule Text Text
funAppRule =
  Rule
    (T "fun" [V "$1", V "$2"])
    (T "app" [V "$3", V "$4"])
    (Net [ Comp (V "$1") (V "$3")
         , Comp (V "$2") (V "$4")
         ])

-- * Argument handling

isFlag :: String -> Bool
isFlag ('-':_) = True
isFlag _       = False

partitionArgs :: [String] -> ([String], [String])
partitionArgs = uncurry f . second (drop 1) . break (== "--")
  where
    f xs ys =
      let (fs, as) = partition isFlag xs
      in
      (fs, as ++ ys)

-- * Main

main :: IO ()
main = do
  (flags, arguments) <- partitionArgs <$> getArgs
  let options = Options ("--omit-fun-app-rule" `notElem` flags)
  when ("--help" `elem` flags) $ do
    progName <- getProgName
    putStrLn $ "Usage: " ++ progName ++ " [--omit-fun-app-rule] file1.prog [file2.prog] ..."
    exitSuccess
  when (null arguments) (die "no arguments given")
  mapM_ (compile options) arguments
  exitSuccess
