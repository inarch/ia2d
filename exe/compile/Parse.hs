module Parse (
    Expr(..)
  , parseFile
  ) where

import           Prelude            hiding (abs)

import           Language.Pin.Term

import           Data.Char          (isLower)
import           Data.Text          (Text)
import qualified Data.Text          as T
import           Text.Parsec
import           Text.Parsec.String

mapFun :: (a -> b) -> Term a c -> Term b c
mapFun f (T t ts) = T (f t) (map (mapFun f) ts)
mapFun _ (V x)    = V x

mapVar :: (b -> c) -> Term a b -> Term a c
mapVar f (T t ts) = T t (map (mapVar f) ts)
mapVar f (V x)    = V (f x)

-- * Data types

data Expr
  = Var Text        -- x
  | App Text [Expr] -- f(e_1, ..., e_n)
  | Con Text [Expr] -- C(e_1, ..., e_n)
  | Abs Text Expr   -- \x -> e
  deriving (Eq, Ord, Show)

-- * Parser

symbol :: String -> Parser String
symbol s = string s <* spaces

comma :: Parser String
comma = symbol ","

commaSep :: Parser a -> Parser [a]
commaSep p = p `sepBy` comma

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

identifier :: Parser String
identifier = do
  let startChar = letter <|> char '_' <|> char '-'
  c  <- startChar
  cs <- many (startChar <|> digit)
  ps <- many $ char '\''
  spaces
  return $ c : (cs ++ ps)

term :: Parser (Term String String)
term = try (V <$> var) <|> fun

var :: Parser String
var = identifier <* notFollowedBy (char '(') <* spaces

fun :: Parser (Term String String)
fun = do
  t <- identifier
  ts <- parens $ commaSep term
  return $ T t ts

type Def = (Term Text Text, Expr)

def :: Parser Def
def = do
  t <- term
  symbol "="
  u <- expr
  symbol ";"
  return (mapVar T.pack . mapFun T.pack $ t, u)

simple :: Parser [Def]
simple = many def

abs :: Parser Expr
abs = do
  char '\\' >> spaces
  x <- T.pack <$> var
  string "->" >> spaces
  e <- expr
  return $ Abs x e

app :: Parser Expr
app =
  do
    t <- T.pack <$> identifier
    ts <- parens $ commaSep expr
    return $ (if isApp t then App else Con) t ts
  where
    isApp = T.all isLower . T.take 1

expr :: Parser Expr
expr = abs <|> try ((Var . T.pack) <$> var) <|> app

parseFile :: FilePath -> IO (Either String [Def])
parseFile path = do
  c <- readFile path
  return $ either (Left . show) Right (parse (simple <* eof) mempty c)
