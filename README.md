# ia2d

`ia2d` is an interpreter for interaction net programs.
Internally `ia2d` translates a given input program into an [interaction automaton](http://drops.dagstuhl.de/opus/volltexte/2016/5989/) (DOI: 10.4230/LIPIcs.FSCD.2016.35) with memory locations arranged on a 2D plane (grid), and then simulates its parallel execution, producing a final report that shows the speed-up achieved compared to a sequential run.

The software is useful for studying interaction nets and their parallel execution and features graphical output in the form of SVG files that can be used to inspect execution details.

In addition a small compiler called `inc` ("interaction-net compiler") is provided that can be used to translate a basic functional programming language into interaction net syntax.
This can be useful for people unfamiliar with programming interaction nets directly.

## Building and installing

The interpreter is written in Haskell and its source code repository is available [here](https://bitbucket.org/inarch/ia2d).
In order to compile it you will need a recent Haskell development environment installed on your computer.
It's recommended to use the [Stack](https://docs.haskellstack.org/) tool for building the project.
Please follow the instructions on the website on how to install Stack on your machine, then proceed with the instructions under "Build using Stack".

If you do not want to use Stack, instructions on how to compile the project using the Cabal build system are given below under "Build using Cabal".

### Build using Stack

If you have the Stack tool installed, check out the `ia2d` source code and move to the root of it.
Then issue the `stack build` command.

    $ git clone https://bitbucket.org/inarch/ia2d
    $ cd ia2d
    $ stack build

The `ia2d` and `inc` binaries are now compiled and can be executed using `stack exec`.

    $ stack exec -- ia2d examples/list_example.inet
    ...

    $ stack exec -- inc examples/nat_unary_example.prog
    ...

If you want to install the binaries, use the `stack install` command.

    $ stack install

The binaries are then installed to `$HOME/.local/bin`.
Add this directory to your `$PATH` environment variable, if you have not already.

### Build using Cabal

If you do not want to use Stack, you must have a recent Glasgow Haskell Compiler (GHC) and the Cabal build system installed on your machine.
If your operating system does not include a package manager or no GHC or Cabal packages are provided, you may find help [here](https://www.haskell.org/platform/).

The `ia2d` interpreter source code is packaged as Cabal package for easy installation.
It's recommended to use a Cabal sandbox for installing dependencies.
You can proceed and install without a sandbox, but might run into issues with already installed versions of dependencies.

If you want to use a sandbox, create one using the following command.

    $ cabal sandbox init

To delete the sandbox later, use `cabal sandbox delete` or delete `cabal.sandbox.config` and the directory `.cabal.sandbox`.

In order build the interpreter, a preliminary step is to configure and install dependencies.

    $ cabal configure

If you get noticed about any missing dependencies, install them and repeat the configuration step.

    $ cabal install <dep1> <dep2> ... <depN>
    $ cabal configure

Finally build and, optionally, install the package.

    $ cabal build
    $ cabal install

If you do not issue the `cabal install` command, the program will be available under `dist/build/ia2d`.

## Usage

To interpret a program using `ia2d`, supply it as a command-line argument as follows:

    $ ia2d examples/bsort_example.inet

The behavior of the interpreter can be controlled by providing additional flags.
Program execution is performed on a 2D grid, the size of which influences both available memory and parallel computation power.
The size of the grid can be changed using the `--grid-size` option.

    $ ia2d --grid-size=128x128 examples/bsort_example.inet

The maximum length of connections between nodes can be set using `--link-range`.
Note that large values will enhance the performance of the simulation, while also making the simulation "unrealistic".
In practice we can hope to achieve massive parallelism only when we have limited shared memory.

Nodes on the grid are constantly rearranged (migrated).
The options `--hop-range` and `--spread-distance` can be used to alter the behaviour of the migration pass.
The hop range limits the range of neighboring locations to which nodes can be moved.

The program will produce graphical output in the SVG format if a `--svg=` option is supplied.
Given the flag `--svg=bsort-.svg` the output is written to files `bsort-0.svg`, `bsort-0m.svg` (the current state after input/output and migration passes), `bsort-1.svg` (the current state after one reduction pass for interaction redexes), `bsort-1m.svg`, and so on.

The complete list of command-line arguments can be obtained via `ia2d --help`.

## Input language

As input language a flat representation of interaction net graphs is used.
A program is written as a set of rules that describe how active pairs appearing in the net can be rewritten, and a number of input nets.
We write the rule for an active pair between nodes `alpha` and `beta` as `alpha(x_1, ..., x_n) >< beta(y_1, ..., y_m) => [N]`, where each variable occurs exactly twice and the net `[N]` is a list of net components, which can be

- wires between ports, e.g. `x ~ y`,
- connections to nodes, e.g. `x ~ gamma(y_1, ..., y_k)`, or
- active pairs, e.g. `alpha(x_1, ..., x_n) ~ beta(y_1, ..., y_m)`.

The language for rules (`RULE`) and nets (`NET`) is defined according to the grammar

    RULE ::= FUN >< FUN => NET
    FUN  ::= f(TERM, ..., TERM)
    TERM ::= x | FUN
    NET  ::= COMP, ..., COMP
    COMP ::= TERM ~ TERM

where `x` and `f` are identifiers.
Identifiers are strings of characters (letters, numbers and the underscore sign) that must start in a character or underscore, i.e. `x`, `y1`, `y2`, `cons`, `nil`, `zip3` or `_42`.

A complete input file consists of a list of definitions and nets, each terminated by a semi-colon.
The syntax is best grasped by looking at some examples.

### Example: compute sum of list of natural numbers

We can encode natural numbers using two constructor terms, `zero()` and `succ(x)`, representing the number 0 and the successor of number `x` respectively.
The number 4 can thus be represented as `succ(succ(succ(succ(zero()))))`.

Similarly we can encode lists using the constructors `cons(x,xs)` and `nil()`.
The list of numbers 0 to 2 is represented as

    cons(zero(), cons(succ(zero()), cons(succ(succ(zero())), nil())))

Now, let's look at the rules we need for traversing the list and summing up the numbers.

The following two rules implement addition of two numbers in the above encoding.

    add(y,r) >< zero() => r~y;
    add(y,r) >< succ(x) => r~succ(t), add(y,t)~x;

We add the next two rules to implement the `sum` operation on lists of natural numbers.

    sum(r) >< nil() => r~zero();
    sum(r) >< cons(x, xs) => x~add(t, r), xs~sum(t);

Note that `sum` is defined in terms of `add`.

To be able to try the implementation we edit a file `sum.inet`.
We add the definitions that implement `add` and `sum`, as well as a net that represents the input to the program.

    add(y,r) >< zero() => r~y;
    add(y,r) >< succ(x) => r~succ(t), add(y,t)~x;

    sum(r) >< nil() => r~zero();
    sum(r) >< cons(x, xs) => x~add(t, r), xs~sum(t);

    xs ~ cons(zero(), cons(succ(zero()), cons(succ(succ(zero())), nil()))),
    sum(r) ~ xs;

The name `xs` is used to label the list of numbers, the name `r` is used to label the output of the program, i.e. the result.

Now we can run the program and obtain a result.

    $ ia2d sum.inet
    seed: 1712572647 2147483398
    SEQ STEPS: 25
    PAR STEPS: 9
    SPEEDUP: 2.78
    PASSES BREAKDOWN:
      2 x 1 fired
      2 x 2 fired
      1 x 3 fired
      2 x 4 fired
      1 x 7 fired
    DURATION:
      0.010024144s
    OUTPUT:
      r~succ^3(zero())

### Modules

For larger programs, it's desirable to split the source code into modules, which then can be reused in different contexts.
By storing the reusable definitions of `add` and `sum` in a file called `nat_unary.inet`, we can use them in a main program as follows.

    import "nat_unary.inet"

    xs ~ cons(zero(), cons(succ(zero()), cons(succ(succ(zero())), nil()))),
    sum(r) ~ xs;

The `import` directive reads the file `nat_unary.inet` and imports all its rules.
Note that we can have multiple `import` directives, but all must precede the definitions and the input net.

Many example programs are provided in the directory `examples` and can be used as inputs to `ia2d`.
Please refer to these examples to get a grip on how this language can be used to construct programs.
